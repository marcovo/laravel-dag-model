<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\Extensions\WithTopologicalOrdering;

/**
 * @mixin Builder
 */
class Situation5Vertex extends DagVertexModel
{
    use WithTopologicalOrdering;

    protected $table = 'situation_5_vertex';

    public function getEdgeModel(): IsEdgeInDagContract
    {
        return new Situation5Edge();
    }
}
