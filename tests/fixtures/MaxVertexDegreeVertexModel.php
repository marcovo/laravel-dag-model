<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\Extensions\WithMaxVertexDegree;
use Marcovo\LaravelDagModel\Models\IsVertexInDagContract;

/**
 * @mixin Builder
 */
class MaxVertexDegreeVertexModel extends Situation1Vertex
{
    use WithMaxVertexDegree;

    public static ?int $maxInDegree = null;
    public static ?int $maxOutDegree = null;

    public function maxInDegree(): ?int
    {
        return static::$maxInDegree;
    }

    public function maxOutDegree(): ?int
    {
        return static::$maxOutDegree;
    }

    public function getEdgeModel(): IsEdgeInDagContract
    {
        return new class extends Situation1Edge
        {
            public function getVertexModel(): IsVertexInDagContract
            {
                return new MaxVertexDegreeVertexModel();
            }
        };
    }
}
