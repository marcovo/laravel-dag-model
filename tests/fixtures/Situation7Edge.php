<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\Edge\ForestAlgorithmEdge;
use Marcovo\LaravelDagModel\Models\IsVertexInDagContract;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\ForestAlgorithm;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\TransitiveClosureAlgorithmContract;

/**
 * @mixin Builder
 */
class Situation7Edge extends ForestAlgorithmEdge
{
    protected $table = 'situation_7_edge';

    public function getVertexModel(): IsVertexInDagContract
    {
        return new Situation7Vertex();
    }
}
