<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\Edge\ForestAlgorithmEdge;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\IsVertexInDagContract;

/**
 * @mixin Builder
 */
class SituationForestAlgorithmStringEdge extends ForestAlgorithmEdge
{
    protected $table = 'situation_forest_algorithm_string_edge';

    public function getVertexModel(): IsVertexInDagContract
    {
        return new class extends DagVertexModel {
            protected $keyType = 'string';

            public function getEdgeModel(): IsEdgeInDagContract
            {
                throw new \Exception();
            }
        };
    }
}
