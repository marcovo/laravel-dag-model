<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\Edge\DlswAlgorithmEdge;
use Marcovo\LaravelDagModel\Models\IsVertexInDagContract;

/**
 * @mixin Builder
 */
class Situation7DlswEdge extends DlswAlgorithmEdge
{
    protected $table = 'situation_7_edge';

    public function getVertexModel(): IsVertexInDagContract
    {
        return new Situation7Vertex();
    }
}
