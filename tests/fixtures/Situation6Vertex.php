<?php

namespace Marcovo\LaravelDagModel\Tests\fixtures;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\Extensions\WithTopologicalOrdering;

/**
 * @mixin Builder
 */
class Situation6Vertex extends DagVertexModel
{
    use WithTopologicalOrdering;

    protected $table = 'situation_6_vertex';

    public function getEdgeModel(): IsEdgeInDagContract
    {
        return new Situation6Edge();
    }

    public function getSeparationCondition(): ?\Closure
    {
        return function ($query) {
            $query->where('separation_column', '=', $this->separation_column);
        };
    }
}
