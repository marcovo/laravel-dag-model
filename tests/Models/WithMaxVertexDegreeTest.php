<?php

namespace Marcovo\LaravelDagModel\Tests\Models;

use Marcovo\LaravelDagModel\Exceptions\InDegreeException;
use Marcovo\LaravelDagModel\Exceptions\OutDegreeException;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\Extensions\WithMaxVertexDegree;
use Marcovo\LaravelDagModel\Tests\fixtures\MaxVertexDegreeVertexModel;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation1Edge;
use Marcovo\LaravelDagModel\Tests\TestCase;

class WithMaxVertexDegreeTest extends TestCase
{
    public function testDefaultDegreesAreNull()
    {
        $instance = new class extends DagVertexModel {
            use WithMaxVertexDegree;

            public function getEdgeModel(): IsEdgeInDagContract
            {
                return new Situation1Edge();
            }
        };

        $this->assertNull($instance->maxInDegree());
        $this->assertNull($instance->maxOutDegree());
    }

    public function testMaxInDegree()
    {
        MaxVertexDegreeVertexModel::$maxInDegree = 3;
        MaxVertexDegreeVertexModel::$maxOutDegree = null;

        $child = MaxVertexDegreeVertexModel::create();
        $parent1 = MaxVertexDegreeVertexModel::create();
        $parent2 = MaxVertexDegreeVertexModel::create();
        $parent3 = MaxVertexDegreeVertexModel::create();
        $parent4 = MaxVertexDegreeVertexModel::create();

        $child->parents()->attach($parent1);
        $child->parents()->attach($parent2);
        $child->parents()->attach($parent3);

        $this->expectException(InDegreeException::class);
        $child->parents()->attach($parent4);
    }

    public function testMaxOutDegree()
    {
        MaxVertexDegreeVertexModel::$maxInDegree = null;
        MaxVertexDegreeVertexModel::$maxOutDegree = 3;

        $parent = MaxVertexDegreeVertexModel::create();
        $child1 = MaxVertexDegreeVertexModel::create();
        $child2 = MaxVertexDegreeVertexModel::create();
        $child3 = MaxVertexDegreeVertexModel::create();
        $child4 = MaxVertexDegreeVertexModel::create();

        $parent->children()->attach($child1);
        $parent->children()->attach($child2);
        $parent->children()->attach($child3);

        $this->expectException(OutDegreeException::class);
        $parent->children()->attach($child4);
    }
}
