<?php

namespace Marcovo\LaravelDagModel\Tests\Models;

use Illuminate\Database\Eloquent\Collection;
use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation3Vertex;
use Marcovo\LaravelDagModel\Tests\TestCase;

class BelongsToManyInGraphTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Situation3Vertex::unguard();
    }

    public function testSave()
    {
        $grandparent = Situation3Vertex::create();
        $parent = Situation3Vertex::create();
        $grandparent->children()->attach($parent);

        $child = Situation3Vertex::make();

        $parent->children()->save($child, ['dummy_edge_column' => 'foo']);

        $children = $parent->children()->withPivot('dummy_edge_column')->get();
        $parents = $child->parents()->withPivot('dummy_edge_column')->get();
        $this->assertCount(1, $children);
        $this->assertCount(1, $parents);
        $this->assertTrue($parent->is($parents[0]));
        $this->assertTrue($child->is($children[0]));
        $this->assertSame('foo', $parents[0]->pivot->dummy_edge_column);
        $this->assertSame('foo', $children[0]->pivot->dummy_edge_column);

        $this->assertCount(2, $grandparent->descendants()->get());
    }

    public function testSaveMany()
    {
        $grandparent = Situation3Vertex::create();
        $parent = Situation3Vertex::create();
        $grandparent->children()->attach($parent);

        $child1 = Situation3Vertex::make();
        $child2 = Situation3Vertex::make();
        $child3 = Situation3Vertex::make();

        $parent->children()->saveMany([
            $child1,
            $child2,
            $child3,
        ]);

        $children = $parent->children()->orderBy('id')->get();
        $parents1 = $child1->parents()->get();
        $parents2 = $child2->parents()->get();
        $parents3 = $child3->parents()->get();
        $this->assertCount(3, $children);
        $this->assertCount(1, $parents1);
        $this->assertCount(1, $parents2);
        $this->assertCount(1, $parents3);
        $this->assertTrue($parent->is($parents1[0]));
        $this->assertTrue($parent->is($parents2[0]));
        $this->assertTrue($parent->is($parents2[0]));
        $this->assertTrue($child1->is($children[0]));
        $this->assertTrue($child2->is($children[1]));
        $this->assertTrue($child3->is($children[2]));

        $this->assertCount(1, $grandparent->children()->get());
        $this->assertCount(4, $grandparent->descendants()->get());
    }

    public function testCreate()
    {
        $parent = Situation3Vertex::create();
        $child = Situation3Vertex::create();
        $parent->children()->attach($child);

        $child->children()->create([
            'dummy_vertex_column' => 'foo',
        ]);

        $children = $child->children()->get();
        $this->assertCount(1, $children);
        $this->assertSame('foo', $children[0]->dummy_vertex_column);

        $this->assertCount(2, $parent->descendants()->get());
    }

    public function testCreateMany()
    {
        $parent = Situation3Vertex::create();
        $child = Situation3Vertex::create();
        $parent->children()->attach($child);

        $child->children()->create();
        $child->children()->create();
        $child->children()->create();

        $this->assertCount(3, $child->children()->get());
        $this->assertCount(1, $parent->children()->get());
        $this->assertCount(4, $parent->descendants()->get());
    }

    public function testToggle()
    {
        $grandparent = Situation3Vertex::create();
        $parent = Situation3Vertex::create();
        $grandparent->children()->attach($parent);

        $child1 = Situation3Vertex::create();
        $child2 = Situation3Vertex::create();
        $parent->children()->attach($child1);

        $changes = $parent->children()->toggle(Collection::make([
            $child1,
            $child2,
        ]));

        $this->assertSame([
            'attached' => [$child2->id],
            'detached' => [$child1->id],
        ], $changes);

        $children = $parent->children()->get();
        $this->assertCount(1, $children);
        $this->assertTrue($child2->is($children[0]));

        $descendants = $grandparent->descendants()->wherePivot('edge_type', '=', IsEdgeInDagContract::TYPE_CLOSURE_EDGE)->get();
        $this->assertCount(1, $descendants);
        $this->assertTrue($child2->is($descendants[0]));
    }

    public function testSync()
    {
        $grandparent = Situation3Vertex::create();
        $parent = Situation3Vertex::create();
        $grandparent->children()->attach($parent);

        $child1 = Situation3Vertex::create();
        $child2 = Situation3Vertex::create();
        $child3 = Situation3Vertex::create();
        $parent->children()->attach($child1);

        $changes = $parent->children()->sync(Collection::make([
            $child2,
            $child3,
        ]));

        $this->assertSame([
            'attached' => [$child2->id, $child3->id],
            'detached' => [$child1->id],
            'updated' => [],
        ], $changes);

        $children = $parent->children()->orderBy('id')->get();
        $this->assertCount(2, $children);
        $this->assertTrue($child2->is($children[0]));
        $this->assertTrue($child3->is($children[1]));

        $descendants = $grandparent->descendants()->wherePivot('edge_type', '=', IsEdgeInDagContract::TYPE_CLOSURE_EDGE)->orderBy('id')->get();
        $this->assertCount(2, $descendants);
        $this->assertTrue($child2->is($descendants[0]));
        $this->assertTrue($child3->is($descendants[1]));
    }

    public function testSyncWithUpdates()
    {
        $parent = Situation3Vertex::create();
        $child1 = Situation3Vertex::create();
        $child2 = Situation3Vertex::create();
        $parent->children()->attach($child1);

        $changes = $parent->children()->sync([
            $child1->id => ['dummy_edge_column' => 'foo'],
            $child2->id => ['dummy_edge_column' => 'bar'],
        ]);

        $this->assertSame([
            'attached' => [$child2->id],
            'detached' => [],
            'updated' => [$child1->id],
        ], $changes);

        $children = $parent->children()->orderBy('id')->withPivot('dummy_edge_column')->get();
        $this->assertCount(2, $children);
        $this->assertSame('foo', $children[0]->pivot->dummy_edge_column);
        $this->assertSame('bar', $children[1]->pivot->dummy_edge_column);
    }

    public function testUpdateExistingPivot()
    {
        $parent = Situation3Vertex::create();
        $child = Situation3Vertex::create();
        $parent->children()->attach($child);

        $updates = $parent->children()->updateExistingPivot($child, ['dummy_edge_column' => 'foo']);

        $this->assertSame(1, $updates);

        $children = $parent->children()->withPivot('dummy_edge_column')->get();
        $this->assertCount(1, $children);
        $this->assertSame('foo', $children[0]->pivot->dummy_edge_column);
    }

    public function testAttach()
    {
        $grandparent = Situation3Vertex::create();
        $parent = Situation3Vertex::create();
        $child = Situation3Vertex::create();
        $grandparent->children()->attach($parent);

        $parent->children()->attach($child);

        $descendants = $parent->children()->get();
        $this->assertCount(1, $descendants);
        $this->assertTrue($child->is($descendants[0]));

        $descendants = $grandparent->descendants()->orderBy('id')->get();
        $this->assertCount(2, $descendants);
        $this->assertTrue($parent->is($descendants[0]));
        $this->assertTrue($child->is($descendants[1]));
    }

    public function testDetach()
    {
        $grandparent = Situation3Vertex::create();
        $parent = Situation3Vertex::create();
        $child = Situation3Vertex::create();
        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);

        $this->assertCount(1, $parent->children()->get());
        $this->assertCount(2, $grandparent->descendants()->get());

        $parent->children()->detach($child);

        $this->assertCount(0, $parent->children()->get());
        $this->assertCount(1, $grandparent->descendants()->get());

        $grandparent->children()->detach($parent);

        $this->assertCount(0, $parent->children()->get());
        $this->assertCount(0, $grandparent->descendants()->get());
    }

    public function testCannotCallInsert()
    {
        $vertex = Situation3Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->children()->insert([]);
    }

    public function testCannotCallInsertOrIgnore()
    {
        $vertex = Situation3Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->children()->insertOrIgnore([]);
    }

    public function testCannotCallInsertGetId()
    {
        $vertex = Situation3Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->children()->insertGetId([]);
    }

    public function testCannotCallInsertUsing()
    {
        $vertex = Situation3Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->children()->insertUsing([], '');
    }

    public function testCannotCallUpdateOrInsert()
    {
        $vertex = Situation3Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->children()->updateOrInsert([]);
    }

    public function testCannotCallDelete()
    {
        $vertex = Situation3Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->children()->delete();
    }

    public function testCannotCallForceDelete()
    {
        $vertex = Situation3Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->children()->forceDelete();
    }
}
