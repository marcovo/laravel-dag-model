<?php

namespace Marcovo\LaravelDagModel\Tests\Models;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Exceptions\InDegreeException;
use Marcovo\LaravelDagModel\Exceptions\OutDegreeException;
use Marcovo\LaravelDagModel\Models\Relations\BelongsToManyInTransitiveClosure;
use Marcovo\LaravelDagModel\Models\Relations\BelongsToManyInTransitiveClosureWithoutSelfLoops;
use Marcovo\LaravelDagModel\Tests\fixtures\MaxVertexDegreeSelfLoopsVertexModel;
use Marcovo\LaravelDagModel\Tests\fixtures\SelfLoopsStringVertexModel;
use Marcovo\LaravelDagModel\Tests\fixtures\SelfLoopsVertexModel;
use Marcovo\LaravelDagModel\Tests\fixtures\SeparatedSelfLoopsVertexModel;
use Marcovo\LaravelDagModel\Tests\TestCase;

class WithSelfLoopsTest extends TestCase
{
    public function testAncestorsHasReplacedRelationClasses()
    {
        $this->assertSame(
            BelongsToManyInTransitiveClosureWithoutSelfLoops::class,
            get_class(SelfLoopsVertexModel::make()->ancestors())
        );

        $this->assertSame(
            BelongsToManyInTransitiveClosure::class,
            get_class(SelfLoopsVertexModel::make()->ancestorsWithSelf())
        );
    }

    public function testDescendantsHasReplacedRelationClasses()
    {
        $this->assertSame(
            BelongsToManyInTransitiveClosureWithoutSelfLoops::class,
            get_class(SelfLoopsVertexModel::make()->descendants())
        );

        $this->assertSame(
            BelongsToManyInTransitiveClosure::class,
            get_class(SelfLoopsVertexModel::make()->descendantsWithSelf())
        );
    }

    public function testCreatesSelfLoops()
    {
        $edgeModel = (new SelfLoopsVertexModel())->getEdgeModel();

        $this->assertEquals([
        ], $edgeModel->newQuery()->get(['start_vertex', 'end_vertex'])->sortBy('start_vertex')->values()->toArray());

        $vertex1 = SelfLoopsVertexModel::create();

        $this->assertEquals([
            ['start_vertex' => $vertex1->id, 'end_vertex' => $vertex1->id],
        ], $edgeModel->newQuery()->get(['start_vertex', 'end_vertex'])->sortBy('start_vertex')->values()->toArray());

        $vertex2 = SelfLoopsVertexModel::create();

        $this->assertEquals([
            ['start_vertex' => $vertex1->id, 'end_vertex' => $vertex1->id],
            ['start_vertex' => $vertex2->id, 'end_vertex' => $vertex2->id],
        ], $edgeModel->newQuery()->get(['start_vertex', 'end_vertex'])->sortBy('start_vertex')->values()->toArray());
    }

    public function testDeletesSelfLoops()
    {
        $edgeModel = (new SelfLoopsVertexModel())->getEdgeModel();

        $vertex1 = SelfLoopsVertexModel::create();
        $vertex2 = SelfLoopsVertexModel::create();
        $vertex3 = SelfLoopsVertexModel::create();

        $this->assertEquals([
            ['start_vertex' => $vertex1->id, 'end_vertex' => $vertex1->id],
            ['start_vertex' => $vertex2->id, 'end_vertex' => $vertex2->id],
            ['start_vertex' => $vertex3->id, 'end_vertex' => $vertex3->id],
        ], $edgeModel->newQuery()->get(['start_vertex', 'end_vertex'])->sortBy('start_vertex')->values()->toArray());

        $vertex2->delete();

        $this->assertEquals([
            ['start_vertex' => $vertex1->id, 'end_vertex' => $vertex1->id],
            ['start_vertex' => $vertex3->id, 'end_vertex' => $vertex3->id],
        ], $edgeModel->newQuery()->get(['start_vertex', 'end_vertex'])->sortBy('start_vertex')->values()->toArray());

        $vertex3->delete();

        $this->assertEquals([
            ['start_vertex' => $vertex1->id, 'end_vertex' => $vertex1->id],
        ], $edgeModel->newQuery()->get(['start_vertex', 'end_vertex'])->sortBy('start_vertex')->values()->toArray());

        $vertex1->delete();

        $this->assertEquals([
        ], $edgeModel->newQuery()->get(['start_vertex', 'end_vertex'])->sortBy('start_vertex')->values()->toArray());
    }

    public function testCanGetAncestorsWithSelf()
    {
        $grandparent = SelfLoopsVertexModel::create();
        $parent = SelfLoopsVertexModel::create();
        $child = SelfLoopsVertexModel::create();
        $grandchild = SelfLoopsVertexModel::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $this->assertSame([
            $grandparent->id,
            $parent->id,
            $child->id,
        ], $child->ancestorsWithSelf->sortBy('id')->pluck('id')->values()->toArray());

        $this->assertSame([
            $grandparent->id,
            $parent->id,
        ], $child->ancestors->sortBy('id')->pluck('id')->values()->toArray());
    }

    public function testCanGetDescendantsWithSelf()
    {
        $grandparent = SelfLoopsVertexModel::create();
        $parent = SelfLoopsVertexModel::create();
        $child = SelfLoopsVertexModel::create();
        $grandchild = SelfLoopsVertexModel::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $this->assertSame([
            $parent->id,
            $child->id,
            $grandchild->id,
        ], $parent->descendantsWithSelf->sortBy('id')->pluck('id')->values()->toArray());

        $this->assertSame([
            $child->id,
            $grandchild->id,
        ], $parent->descendants->sortBy('id')->pluck('id')->values()->toArray());
    }

    public function testCanGetWhereHasAncestorsWithSelf()
    {
        $grandparent = SelfLoopsVertexModel::create();
        $parent = SelfLoopsVertexModel::create();
        $child = SelfLoopsVertexModel::create();
        $grandchild = SelfLoopsVertexModel::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $this->assertSame(
            [
                $parent->id,
                $child->id,
                $grandchild->id,
            ],
            SelfLoopsVertexModel::query()
                ->whereHas('ancestorsWithSelf', function (Builder $query) use ($parent) {
                    $query->whereKey($parent->id);
                })
                ->orderBy('id')
                ->pluck('id')
                ->all()
        );

        $this->assertSame(
            [
                $child->id,
                $grandchild->id,
            ],
            SelfLoopsVertexModel::query()
                ->whereHas('ancestors', function (Builder $query) use ($parent) {
                    $query->whereKey($parent->id);
                })
                ->orderBy('id')
                ->pluck('id')
                ->all()
        );
    }

    public function testCanGetWhereHasDescendantsWithSelf()
    {
        $grandparent = SelfLoopsVertexModel::create();
        $parent = SelfLoopsVertexModel::create();
        $child = SelfLoopsVertexModel::create();
        $grandchild = SelfLoopsVertexModel::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $this->assertSame(
            [
                $grandparent->id,
                $parent->id,
                $child->id,
            ],
            SelfLoopsVertexModel::query()
                ->whereHas('descendantsWithSelf', function (Builder $query) use ($child) {
                    $query->whereKey($child->id);
                })
                ->orderBy('id')
                ->pluck('id')
                ->all()
        );

        $this->assertSame(
            [
                $grandparent->id,
                $parent->id,
            ],
            SelfLoopsVertexModel::query()
                ->whereHas('descendants', function (Builder $query) use ($child) {
                    $query->whereKey($child->id);
                })
                ->orderBy('id')
                ->pluck('id')
                ->all()
        );
    }

    public function testWorksWithStringPrimaryKey()
    {
        $grandparent = SelfLoopsStringVertexModel::create();
        $parent = SelfLoopsStringVertexModel::create();
        $child = SelfLoopsStringVertexModel::create();
        $grandchild = SelfLoopsStringVertexModel::create();

        $grandparent->children()->attach($parent);
        $parent->children()->attach($child);
        $child->children()->attach($grandchild);

        $this->assertSame(collect([
            $parent->uuid,
            $child->uuid,
            $grandchild->uuid,
        ])->sort()->values()->toArray(), $parent->descendantsWithSelf()->pluck('uuid')->sort()->values()->toArray());

        $child->children()->detach($grandchild);

        $this->assertSame(collect([
            $parent->uuid,
            $child->uuid,
        ])->sort()->values()->toArray(), $parent->descendantsWithSelf()->pluck('uuid')->sort()->values()->toArray());
    }

    public function testCanRebuildSelfLoops()
    {
        $edgeModel = (new SelfLoopsVertexModel())->getEdgeModel();

        $vertex1 = SelfLoopsVertexModel::create();
        $vertex2 = SelfLoopsVertexModel::create();
        $vertex3 = SelfLoopsVertexModel::create();
        $vertex4 = SelfLoopsVertexModel::create();

        $this->assertEquals(4, $edgeModel->newQuery()->count());

        $edgeModel->newQuery()->where('start_vertex', $vertex2->id)->delete();

        $edgeModel->newQuery()->where('start_vertex', $vertex3->id)->update(['end_vertex' => $vertex4->id]);

        $this->assertEquals(3, $edgeModel->newQuery()->count());

        (new SelfLoopsVertexModel())->rebuildSelfLoops();

        $this->assertEquals([
            ['start_vertex' => $vertex1->id, 'end_vertex' => $vertex1->id],
            ['start_vertex' => $vertex2->id, 'end_vertex' => $vertex2->id],
            ['start_vertex' => $vertex3->id, 'end_vertex' => $vertex3->id],
            ['start_vertex' => $vertex4->id, 'end_vertex' => $vertex4->id],
        ], $edgeModel->newQuery()->get(['start_vertex', 'end_vertex'])->sortBy('start_vertex')->values()->toArray());
    }

    public function testCanRebuildSelfLoopsSeparated()
    {
        SeparatedSelfLoopsVertexModel::unguard();

        $edgeModel = (new SeparatedSelfLoopsVertexModel())->getEdgeModel();

        $vertexFoo1 = SeparatedSelfLoopsVertexModel::create(['separation_column' => 'foo']);
        $vertexFoo2 = SeparatedSelfLoopsVertexModel::create(['separation_column' => 'foo']);
        $vertexFoo3 = SeparatedSelfLoopsVertexModel::create(['separation_column' => 'foo']);
        $vertexFoo4 = SeparatedSelfLoopsVertexModel::create(['separation_column' => 'foo']);

        $vertexBar1 = SeparatedSelfLoopsVertexModel::create(['separation_column' => 'bar']);
        $vertexBar2 = SeparatedSelfLoopsVertexModel::create(['separation_column' => 'bar']);
        $vertexBar3 = SeparatedSelfLoopsVertexModel::create(['separation_column' => 'bar']);
        $vertexBar4 = SeparatedSelfLoopsVertexModel::create(['separation_column' => 'bar']);

        $this->assertEquals(8, $edgeModel->newQuery()->count());

        $edgeModel->newQuery()->where('start_vertex', $vertexFoo2->id)->delete();
        $edgeModel->newQuery()->where('start_vertex', $vertexBar2->id)->delete();

        $edgeModel->newQuery()->where('start_vertex', $vertexFoo3->id)->update(['end_vertex' => $vertexFoo4->id]);
        $edgeModel->newQuery()->where('start_vertex', $vertexBar3->id)->update(['end_vertex' => $vertexBar4->id]);

        $this->assertEquals([
            ['start_vertex' => $vertexFoo1->id, 'end_vertex' => $vertexFoo1->id],
            ['start_vertex' => $vertexFoo3->id, 'end_vertex' => $vertexFoo4->id],
            ['start_vertex' => $vertexFoo4->id, 'end_vertex' => $vertexFoo4->id],

            ['start_vertex' => $vertexBar1->id, 'end_vertex' => $vertexBar1->id],
            ['start_vertex' => $vertexBar3->id, 'end_vertex' => $vertexBar4->id],
            ['start_vertex' => $vertexBar4->id, 'end_vertex' => $vertexBar4->id],
        ], $edgeModel->newQuery()->get(['start_vertex', 'end_vertex'])->sortBy('start_vertex')->values()->toArray());

        $referenceVertex = new SeparatedSelfLoopsVertexModel();
        $referenceVertex->separation_column = 'bar';
        $referenceVertex->rebuildSelfLoops();

        $this->assertEquals([
            ['start_vertex' => $vertexFoo1->id, 'end_vertex' => $vertexFoo1->id],
            ['start_vertex' => $vertexFoo3->id, 'end_vertex' => $vertexFoo4->id],
            ['start_vertex' => $vertexFoo4->id, 'end_vertex' => $vertexFoo4->id],

            ['start_vertex' => $vertexBar1->id, 'end_vertex' => $vertexBar1->id],
            ['start_vertex' => $vertexBar2->id, 'end_vertex' => $vertexBar2->id],
            ['start_vertex' => $vertexBar3->id, 'end_vertex' => $vertexBar3->id],
            ['start_vertex' => $vertexBar4->id, 'end_vertex' => $vertexBar4->id],
        ], $edgeModel->newQuery()->get(['start_vertex', 'end_vertex'])->sortBy('start_vertex')->values()->toArray());

        $referenceVertex = new SeparatedSelfLoopsVertexModel();
        $referenceVertex->separation_column = 'foo';
        $referenceVertex->rebuildSelfLoops();

        $this->assertEquals([
            ['start_vertex' => $vertexFoo1->id, 'end_vertex' => $vertexFoo1->id],
            ['start_vertex' => $vertexFoo2->id, 'end_vertex' => $vertexFoo2->id],
            ['start_vertex' => $vertexFoo3->id, 'end_vertex' => $vertexFoo3->id],
            ['start_vertex' => $vertexFoo4->id, 'end_vertex' => $vertexFoo4->id],

            ['start_vertex' => $vertexBar1->id, 'end_vertex' => $vertexBar1->id],
            ['start_vertex' => $vertexBar2->id, 'end_vertex' => $vertexBar2->id],
            ['start_vertex' => $vertexBar3->id, 'end_vertex' => $vertexBar3->id],
            ['start_vertex' => $vertexBar4->id, 'end_vertex' => $vertexBar4->id],
        ], $edgeModel->newQuery()->get(['start_vertex', 'end_vertex'])->sortBy('start_vertex')->values()->toArray());
    }

    public function testMaxInDegreeExcludesSelfLoops()
    {
        MaxVertexDegreeSelfLoopsVertexModel::$maxInDegree = 3;
        MaxVertexDegreeSelfLoopsVertexModel::$maxOutDegree = null;

        $child = MaxVertexDegreeSelfLoopsVertexModel::create();
        $parent1 = MaxVertexDegreeSelfLoopsVertexModel::create();
        $parent2 = MaxVertexDegreeSelfLoopsVertexModel::create();
        $parent3 = MaxVertexDegreeSelfLoopsVertexModel::create();
        $parent4 = MaxVertexDegreeSelfLoopsVertexModel::create();

        $child->parents()->attach($parent1);
        $child->parents()->attach($parent2);
        $child->parents()->attach($parent3);

        // 3 graph edges, 0 TC edges, 5 loop edges
        $this->assertSame(8, $child->getEdgeModel()->newQuery()->count());

        $this->expectException(InDegreeException::class);
        $child->parents()->attach($parent4);
    }

    public function testMaxOutDegreeExcludesSelfLoops()
    {
        MaxVertexDegreeSelfLoopsVertexModel::$maxInDegree = null;
        MaxVertexDegreeSelfLoopsVertexModel::$maxOutDegree = 3;

        $parent = MaxVertexDegreeSelfLoopsVertexModel::create();
        $child1 = MaxVertexDegreeSelfLoopsVertexModel::create();
        $child2 = MaxVertexDegreeSelfLoopsVertexModel::create();
        $child3 = MaxVertexDegreeSelfLoopsVertexModel::create();
        $child4 = MaxVertexDegreeSelfLoopsVertexModel::create();

        $parent->children()->attach($child1);
        $parent->children()->attach($child2);
        $parent->children()->attach($child3);

        // 3 graph edges, 0 TC edges, 5 loop edges
        $this->assertSame(8, $parent->getEdgeModel()->newQuery()->count());

        $this->expectException(OutDegreeException::class);
        $parent->children()->attach($child4);
    }
}
