<?php

namespace Marcovo\LaravelDagModel\Tests\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Marcovo\LaravelDagModel\Exceptions\LaravelDagModelException;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation5Edge;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation5Vertex;
use Marcovo\LaravelDagModel\Tests\TestCase;

class WithTopologicalOrderingTest extends TestCase
{
    public function testSetsOrderOnCreate()
    {
        $parent = Situation5Vertex::create();
        $child = Situation5Vertex::create();
        $grandchild = Situation5Vertex::create();

        $this->assertSame(0, (int)$parent->refresh()->top_order);
        $this->assertSame(1, (int)$child->refresh()->top_order);
        $this->assertSame(2, (int)$grandchild->refresh()->top_order);
    }

    public function testSetsOrderOnDelete()
    {
        $parent = Situation5Vertex::create();
        $child = Situation5Vertex::create();
        $grandchild = Situation5Vertex::create();

        $this->assertSame(0, (int)$parent->refresh()->top_order);
        $this->assertSame(1, (int)$child->refresh()->top_order);
        $this->assertSame(2, (int)$grandchild->refresh()->top_order);

        $child->delete();

        $this->assertSame(0, (int)$parent->refresh()->top_order);
        $this->assertSame(1, (int)$grandchild->refresh()->top_order);
    }

    public function testSetsOrderWhenCorrectAlready()
    {
        $parent = Situation5Vertex::create();
        $child = Situation5Vertex::create();

        $this->assertSame(0, (int)$parent->refresh()->top_order);
        $this->assertSame(1, (int)$child->refresh()->top_order);

        $child->parents()->attach($parent);

        $this->assertSame(0, (int)$parent->refresh()->top_order);
        $this->assertSame(1, (int)$child->refresh()->top_order);
    }

    public function testSetsOrderWhenNotCorrectAlready()
    {
        $child = Situation5Vertex::create();
        $parent = Situation5Vertex::create();

        $this->assertSame(1, (int)$parent->refresh()->top_order);
        $this->assertSame(0, (int)$child->refresh()->top_order);

        $child->parents()->attach($parent);

        $this->assertSame(0, (int)$parent->refresh()->top_order);
        $this->assertSame(1, (int)$child->refresh()->top_order);
    }

    public function testMovesAncestor()
    {
        $child = Situation5Vertex::create();
        $parent = Situation5Vertex::create();
        $grandparent = Situation5Vertex::create();

        $this->assertSame(2, (int)$grandparent->refresh()->top_order);
        $this->assertSame(1, (int)$parent->refresh()->top_order);
        $this->assertSame(0, (int)$child->refresh()->top_order);

        $parent->parents()->attach($grandparent);

        $this->assertSame(1, (int)$grandparent->refresh()->top_order);
        $this->assertSame(2, (int)$parent->refresh()->top_order);
        $this->assertSame(0, (int)$child->refresh()->top_order);

        $child->parents()->attach($parent);

        $this->assertSame(0, (int)$grandparent->refresh()->top_order);
        $this->assertSame(1, (int)$parent->refresh()->top_order);
        $this->assertSame(2, (int)$child->refresh()->top_order);
    }

    public function testMovesDescendant()
    {
        $child = Situation5Vertex::create();
        $parent = Situation5Vertex::create();
        $grandparent = Situation5Vertex::create();

        $this->assertSame(2, (int)$grandparent->refresh()->top_order);
        $this->assertSame(1, (int)$parent->refresh()->top_order);
        $this->assertSame(0, (int)$child->refresh()->top_order);

        $child->parents()->attach($parent);

        $this->assertSame(2, (int)$grandparent->refresh()->top_order);
        $this->assertSame(0, (int)$parent->refresh()->top_order);
        $this->assertSame(1, (int)$child->refresh()->top_order);

        $parent->parents()->attach($grandparent);

        $this->assertSame(0, (int)$grandparent->refresh()->top_order);
        $this->assertSame(1, (int)$parent->refresh()->top_order);
        $this->assertSame(2, (int)$child->refresh()->top_order);
    }

    public function testMovesConsecutiveChain()
    {
        Situation5Vertex::create();
        $A1 = Situation5Vertex::create();
        $A2 = Situation5Vertex::create();
        $A3 = Situation5Vertex::create();
        $A4 = Situation5Vertex::create();
        Situation5Vertex::create();
        Situation5Vertex::create();
        $B1 = Situation5Vertex::create();
        $B2 = Situation5Vertex::create();
        $B3 = Situation5Vertex::create();
        $B4 = Situation5Vertex::create();
        Situation5Vertex::create();

        $A1->children()->attach($A2);
        $A2->children()->attach($A3);
        $A3->children()->attach($A4);
        $B1->children()->attach($B2);
        $B2->children()->attach($B3);
        $B3->children()->attach($B4);

        $fetchTopOrder = fn () => DB::table('situation_5_vertex')
            ->orderBy('id')
            ->pluck('top_order', 'id')
            ->map(fn ($x) => intval($x))
            ->values()
            ->all();

        $this->assertSame([
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
        ], $fetchTopOrder());

        $B4->children()->attach($A1);

        $this->assertSame([
            0,
            5,
            6,
            7,
            8,
            9,
            10,
            1,
            2,
            3,
            4,
            11,
        ], $fetchTopOrder());
    }

    public function testMovesScatteredChain()
    {
        Situation5Vertex::create();
        $A1 = Situation5Vertex::create();
        Situation5Vertex::create();
        $A2 = Situation5Vertex::create();
        Situation5Vertex::create();
        $A3 = Situation5Vertex::create();
        Situation5Vertex::create();
        $A4 = Situation5Vertex::create();
        Situation5Vertex::create();
        Situation5Vertex::create();
        $B1 = Situation5Vertex::create();
        Situation5Vertex::create();
        $B2 = Situation5Vertex::create();
        Situation5Vertex::create();
        $B3 = Situation5Vertex::create();
        Situation5Vertex::create();
        $B4 = Situation5Vertex::create();
        Situation5Vertex::create();

        $A1->children()->attach($A2);
        $A2->children()->attach($A3);
        $A3->children()->attach($A4);
        $B1->children()->attach($B2);
        $B2->children()->attach($B3);
        $B3->children()->attach($B4);

        $fetchTopOrder = fn () => DB::table('situation_5_vertex')
            ->orderBy('id')
            ->pluck('top_order', 'id')
            ->map(fn ($x) => intval($x))
            ->values()
            ->all();

        $this->assertSame([
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
        ], $fetchTopOrder());

        $B4->children()->attach($A1);

        $this->assertSame([
            0,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            1,
            14,
            2,
            15,
            3,
            16,
            4,
            17,
        ], $fetchTopOrder());
    }

    public function testMovesHalfScatteredChain()
    {
        Situation5Vertex::create();
        $A1 = Situation5Vertex::create();
        Situation5Vertex::create();
        $A2 = Situation5Vertex::create();
        $A3 = Situation5Vertex::create();
        Situation5Vertex::create();
        $A4 = Situation5Vertex::create();
        Situation5Vertex::create();
        Situation5Vertex::create();
        $B1 = Situation5Vertex::create();
        Situation5Vertex::create();
        $B2 = Situation5Vertex::create();
        $B3 = Situation5Vertex::create();
        Situation5Vertex::create();
        $B4 = Situation5Vertex::create();
        Situation5Vertex::create();

        $A1->children()->attach($A2);
        $A2->children()->attach($A3);
        $A3->children()->attach($A4);
        $B1->children()->attach($B2);
        $B2->children()->attach($B3);
        $B3->children()->attach($B4);

        $fetchTopOrder = fn () => DB::table('situation_5_vertex')
            ->orderBy('id')
            ->pluck('top_order', 'id')
            ->map(fn ($x) => intval($x))
            ->values()
            ->all();

        $this->assertSame([
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
        ], $fetchTopOrder());

        $B4->children()->attach($A1);

        $this->assertSame([
            0,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            1,
            13,
            2,
            3,
            14,
            4,
            15,
        ], $fetchTopOrder());
    }

    /**
     * Build a diagonal diamond
     *       1
     *      / \
     *     2   3
     *     | \ |
     *     4   5
     *      \ /
     *       6
     */
    private function buildDiagonalDiamond()
    {
        Situation5Vertex::create();
        $V6 = Situation5Vertex::create();
        Situation5Vertex::create();
        $V5 = Situation5Vertex::create();
        Situation5Vertex::create();
        $V4 = Situation5Vertex::create();
        Situation5Vertex::create();
        $V3 = Situation5Vertex::create();
        Situation5Vertex::create();
        $V2 = Situation5Vertex::create();
        Situation5Vertex::create();
        $V1 = Situation5Vertex::create();
        Situation5Vertex::create();

        $V1->children()->attach($V2);
        $V1->children()->attach($V3);
        $V2->children()->attach($V4);
        $V2->children()->attach($V5);
        $V3->children()->attach($V5);
        $V4->children()->attach($V6);
        $V5->children()->attach($V6);

        $V1->refresh();
        $V2->refresh();
        $V3->refresh();
        $V4->refresh();
        $V5->refresh();
        $V6->refresh();

        return [$V1, $V2, $V3, $V4, $V5, $V6];
    }

    public function testTopologicalOrderingInDiagonalDiamond()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $this->assertGreaterThan($V1->top_order, $V2->top_order);
        $this->assertGreaterThan($V1->top_order, $V3->top_order);
        $this->assertGreaterThan($V2->top_order, $V4->top_order);
        $this->assertGreaterThan($V2->top_order, $V5->top_order);
        $this->assertGreaterThan($V3->top_order, $V5->top_order);
        $this->assertGreaterThan($V4->top_order, $V6->top_order);
        $this->assertGreaterThan($V5->top_order, $V6->top_order);
    }

    public function testCanGetDescendantsOrdered()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $visitedParents = [$V1->id => true];
        foreach ($V1->descendantsOrdered->load('parents') as $vertex) {
            foreach ($vertex->parents as $parent) {
                if (!isset($visitedParents[$parent->id])) {
                    $this->fail('Failed asserting that a parent has been visited');
                }
            }
            $visitedParents[$vertex->id] = true;
        }
        $this->assertCount(6, $visitedParents);
    }

    public function testCanGetAncestorsOrdered()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $visitedParents = [];
        foreach ($V6->ancestorsOrdered->load('parents') as $vertex) {
            foreach ($vertex->parents as $parent) {
                if (!isset($visitedParents[$parent->id])) {
                    $this->fail('Failed asserting that a parent has been visited');
                }
            }
            $visitedParents[$vertex->id] = true;
        }
        $this->assertCount(5, $visitedParents);
    }

    public function testCanGetAncestorsOrderedDesc()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $visitedChildren = [$V6->id => true];
        foreach ($V6->ancestorsOrderedDesc->load('children') as $vertex) {
            foreach ($vertex->children as $child) {
                if (!isset($visitedChildren[$child->id])) {
                    $this->fail('Failed asserting that a child has been visited');
                }
            }
            $visitedChildren[$vertex->id] = true;
        }
        $this->assertCount(6, $visitedChildren);
    }

    public function testDoesNotReorderOnException()
    {
        $child = Situation5Vertex::create();
        $parent = Situation5Vertex::create();

        $this->assertSame(1, (int)$parent->refresh()->top_order);
        $this->assertSame(0, (int)$child->refresh()->top_order);

        $exception = new class extends LaravelDagModelException {};
        $thrown = false;
        Event::listen(
            'eloquent.saving: ' . Situation5Edge::class,
            function (Situation5Edge $edge) use ($parent, $child, $exception) {
                $this->assertSame(0, (int)$parent->refresh()->top_order);
                $this->assertSame(1, (int)$child->refresh()->top_order);

                throw new $exception();
            }
        );

        try {
            $parent->children()->attach($child);
        } catch (LaravelDagModelException $e) {
            if (! $e instanceof $exception) {
                throw $e;
            } else {
                $thrown = true;
            }
        }


        $this->assertTrue($thrown);

        $this->assertSame(1, (int)$parent->refresh()->top_order);
        $this->assertSame(0, (int)$child->refresh()->top_order);
    }

    public function testCanRebuild()
    {
        Situation5Vertex::create();
        $A1 = Situation5Vertex::create();
        Situation5Vertex::create();
        $A2 = Situation5Vertex::create();
        Situation5Vertex::create();
        $A3 = Situation5Vertex::create();
        Situation5Vertex::create();
        $A4 = Situation5Vertex::create();
        Situation5Vertex::create();
        Situation5Vertex::create();
        $B1 = Situation5Vertex::create();
        Situation5Vertex::create();
        $B2 = Situation5Vertex::create();
        Situation5Vertex::create();
        $B3 = Situation5Vertex::create();
        Situation5Vertex::create();
        $B4 = Situation5Vertex::create();
        Situation5Vertex::create();

        $A1->children()->attach($A2);
        $A2->children()->attach($A3);
        $A3->children()->attach($A4);
        $B1->children()->attach($B2);
        $B2->children()->attach($B3);
        $B3->children()->attach($B4);

        $B4->children()->attach($A1);

        $fetchTopOrder = fn () => DB::table('situation_5_vertex')
            ->orderBy('id')
            ->pluck('top_order', 'id')
            ->map(fn ($x) => intval($x))
            ->values()
            ->all();

        $order = [
            0,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            1,
            14,
            2,
            15,
            3,
            16,
            4,
            17,
        ];

        $this->assertSame($order, $fetchTopOrder());

        DB::table('situation_5_vertex')->update(['top_order' => 0]);

        $B4->rebuildTopologicalOrdering();

        $this->assertSame(17, (int)DB::table('situation_5_vertex')->max('top_order'));

        $this->assertGreaterThan($B1->refresh()->top_order, $B2->refresh()->top_order);
        $this->assertGreaterThan($B2->refresh()->top_order, $B3->refresh()->top_order);
        $this->assertGreaterThan($B3->refresh()->top_order, $B4->refresh()->top_order);
        $this->assertGreaterThan($B4->refresh()->top_order, $A1->refresh()->top_order);
        $this->assertGreaterThan($A1->refresh()->top_order, $A2->refresh()->top_order);
        $this->assertGreaterThan($A2->refresh()->top_order, $A3->refresh()->top_order);
        $this->assertGreaterThan($A3->refresh()->top_order, $A4->refresh()->top_order);
    }
}
