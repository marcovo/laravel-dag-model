<?php

namespace Marcovo\LaravelDagModel\Tests\Models\Builder;

use Illuminate\Database\Eloquent\Builder;
use Marcovo\LaravelDagModel\Exceptions\LaravelDagModelException;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation1Vertex;
use Marcovo\LaravelDagModel\Tests\TestCase;

class QueryBuilderTest extends TestCase
{
    /**
     * Build a diagonal diamond
     *       1
     *      / \
     *     2   3
     *     | \ |
     *     4   5
     *      \ /
     *       6
     */
    private function buildDiagonalDiamond()
    {
        $V1 = Situation1Vertex::create();
        $V2 = Situation1Vertex::create();
        $V3 = Situation1Vertex::create();
        $V4 = Situation1Vertex::create();
        $V5 = Situation1Vertex::create();
        $V6 = Situation1Vertex::create();
        Situation1Vertex::create();
        Situation1Vertex::create();

        $V1->children()->attach($V2);
        $V1->children()->attach($V3);
        $V2->children()->attach($V4);
        $V2->children()->attach($V5);
        $V3->children()->attach($V5);
        $V4->children()->attach($V6);
        $V5->children()->attach($V6);

        $V1->refresh();
        $V2->refresh();
        $V3->refresh();
        $V4->refresh();
        $V5->refresh();
        $V6->refresh();

        return [$V1, $V2, $V3, $V4, $V5, $V6];
    }

    private function getOrderedIds(Builder $query): array
    {
        return $query
            ->orderBy('id')
            ->pluck('id')
            ->map(fn ($x) => intval($x))
            ->all();
    }

    public function testWhereAncestorOf()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $this->assertSame(
            [$V1->id, $V2->id, $V3->id],
            $this->getOrderedIds(Situation1Vertex::query()->whereAncestorOf($V5))
        );
    }

    public function testOrWhereAncestorOf()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $this->assertSame(
            [$V1->id, $V2->id, $V3->id, $V6->id],
            $this->getOrderedIds(
                Situation1Vertex::query()->where('id', '=', $V6->id)->orWhereAncestorOf($V5)
            )
        );
    }

    public function testWhereAncestorOfOrSelf()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $this->assertSame(
            [$V1->id, $V2->id, $V3->id, $V5->id],
            $this->getOrderedIds(Situation1Vertex::query()->whereAncestorOfOrSelf($V5))
        );
    }

    public function testOrWhereAncestorOfOrSelf()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $this->assertSame(
            [$V1->id, $V2->id, $V3->id, $V5->id, $V6->id],
            $this->getOrderedIds(
                Situation1Vertex::query()->where('id', '=', $V6->id)->orWhereAncestorOfOrSelf($V5)
            )
        );
    }

    public function testWhereDescendantOf()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $this->assertSame(
            [$V4->id, $V5->id, $V6->id],
            $this->getOrderedIds(Situation1Vertex::query()->whereDescendantOf($V2))
        );
    }

    public function testOrWhereDescendantOf()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $this->assertSame(
            [$V1->id, $V4->id, $V5->id, $V6->id],
            $this->getOrderedIds(
                Situation1Vertex::query()->where('id', '=', $V1->id)->orWhereDescendantOf($V2)
            )
        );
    }

    public function testWhereDescendantOfOrSelf()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $this->assertSame(
            [$V2->id, $V4->id, $V5->id, $V6->id],
            $this->getOrderedIds(Situation1Vertex::query()->whereDescendantOfOrSelf($V2))
        );
    }

    public function testOrWhereDescendantOfOrSelf()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $this->assertSame(
            [$V1->id, $V2->id, $V4->id, $V5->id, $V6->id],
            $this->getOrderedIds(
                Situation1Vertex::query()->where('id', '=', $V1->id)->orWhereDescendantOfOrSelf($V2)
            )
        );
    }

    public function testWhereAncestorOfUsingId()
    {
        [$V1, $V2, $V3, $V4, $V5, $V6] = $this->buildDiagonalDiamond();

        $this->assertSame(
            [$V1->id, $V2->id, $V3->id],
            $this->getOrderedIds(Situation1Vertex::query()->whereAncestorOf($V5->id))
        );
    }

    public function testFailsOnInvalidId()
    {
        $this->expectException(LaravelDagModelException::class);
        $this->expectExceptionMessage('Received invalid id');
        Situation1Vertex::query()->whereAncestorOf(['foo']);
    }
}
