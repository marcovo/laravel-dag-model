<?php

namespace Marcovo\LaravelDagModel\Tests\Models\Edge;

use Illuminate\Support\Facades\Event;
use Marcovo\LaravelDagModel\Exceptions\LaravelDagModelException;
use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation2Edge;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation2Vertex;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationDlswAlgorithmEdge;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationForestAlgorithmEdge;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationPathCountAlgorithmEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\DlswAlgorithm;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\ForestAlgorithm;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\PathCountAlgorithm;
use PHPUnit\Framework\Attributes\DataProvider;

class IsEdgeInDagTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Situation2Vertex::unguard();
    }

    public function testDlswAlgorithmEdge()
    {
        $this->assertInstanceOf(DlswAlgorithm::class, (new SituationDlswAlgorithmEdge())->getTransitiveClosureAlgorithm());
    }

    public function testPathCountAlgorithmEdge()
    {
        $this->assertInstanceOf(PathCountAlgorithm::class, (new SituationPathCountAlgorithmEdge())->getTransitiveClosureAlgorithm());
    }

    public function testForestAlgorithmEdge()
    {
        $this->assertInstanceOf(ForestAlgorithm::class, (new SituationForestAlgorithmEdge())->getTransitiveClosureAlgorithm());
    }

    public function testCanCreateEdgeWithAttributes()
    {
        $parent = Situation2Vertex::create();
        $child = Situation2Vertex::create();

        $parent->children()->attach([
            $child->id => ['dummy_edge_column' => 'foo'],
        ]);

        $children = $parent->children()->withPivot('dummy_edge_column')->get();
        $this->assertSame('foo', $children[0]->pivot->dummy_edge_column);
    }

    public function testCanUpdateEdgeWithAttributes()
    {
        $parent = Situation2Vertex::create();
        $child = Situation2Vertex::create();
        $parent->children()->attach($child);

        $parent->children()->updateExistingPivot($child, ['dummy_edge_column' => 'foo']);

        $children = $parent->children()->withPivot('dummy_edge_column')->get();
        $this->assertSame('foo', $children[0]->pivot->dummy_edge_column);
    }

    public function testCanDeleteEdge()
    {
        $parent = Situation2Vertex::create();
        $child = Situation2Vertex::create();
        $parent->children()->attach($child);

        $this->assertCount(1, $parent->children()->get());

        $parent->children()->detach($child);

        $this->assertCount(0, $parent->children()->get());
    }

    public function testCannotCreateWithManagedColumns()
    {
        $parent = Situation2Vertex::create();
        $child = Situation2Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $parent->children()->attach([
            $child->id => ['path_count' => 3],
        ]);
    }

    public function testCannotUpdateManagedColumns()
    {
        $parent = Situation2Vertex::create();
        $child = Situation2Vertex::create();
        $parent->children()->attach($child);

        $this->expectException(MethodNotSupportedException::class);
        $parent->children()->updateExistingPivot($child, ['path_count' => 3]);
    }

    public static function providerAttachEvents(): array
    {
        return [
            ['creatingDagEdge'],
            ['updating'],
            ['saving'],
        ];
    }

    /**
     * @dataProvider providerAttachEvents
     */
    #[DataProvider('providerAttachEvents')]
    public function testCanUseAttachEvents(string $event)
    {
        $parent = Situation2Vertex::create();
        $child = Situation2Vertex::create();

        $counter = 0;
        Event::listen(
            'eloquent.' . $event . ': ' . Situation2Edge::class,
            function (Situation2Edge $edge) use (&$counter, $parent, $child) {
                $this->assertSame($parent->id, $edge->start_vertex);
                $this->assertSame($child->id, $edge->end_vertex);
                $counter++;
            }
        );

        $parent->children()->attach($child, ['dummy_edge_column' => 'foo']);

        $this->assertSame(1, $counter);
        $this->assertCount(1, $parent->children()->get());
        $this->assertCount(1, $child->parents()->get());
    }

    /**
     * @dataProvider providerAttachEvents
     */
    #[DataProvider('providerAttachEvents')]
    public function testCanCancelCreateUsingAttachEvents(string $event)
    {
        $parent = Situation2Vertex::create();
        $child = Situation2Vertex::create();

        $counter = 0;
        Event::listen(
            'eloquent.' . $event . ': ' . Situation2Edge::class,
            function (Situation2Edge $edge) use (&$counter, $parent, $child) {
                $this->assertSame($parent->id, $edge->start_vertex);
                $this->assertSame($child->id, $edge->end_vertex);
                $counter++;
                return false;
            }
        );

        $parent->children()->attach($child, ['dummy_edge_column' => 'foo']);

        $this->assertSame(1, $counter);
        $this->assertCount(0, $parent->children()->get());
        $this->assertCount(0, $child->parents()->get());
    }

    public function testCanCancelUpdateUsingEvent()
    {
        $parent = Situation2Vertex::create();
        $child = Situation2Vertex::create();

        $parent->children()->attach($child, ['dummy_edge_column' => 'foo']);

        $counter = 0;
        Event::listen(
            'eloquent.updating: ' . Situation2Edge::class,
            function (Situation2Edge $edge) use (&$counter, $parent, $child) {
                $this->assertSame($parent->id, (int)$edge->start_vertex);
                $this->assertSame($child->id, (int)$edge->end_vertex);
                $counter++;
                return false;
            }
        );

        $parent->children()->updateExistingPivot($child->id, ['dummy_edge_column' => 'bar']);

        $this->assertSame(1, $counter);
        $this->assertSame('foo', $parent->children()->withPivot('dummy_edge_column')->first()->pivot->dummy_edge_column);
    }

    public function testExceptionFallsThroughSave()
    {
        $parent = Situation2Vertex::create();
        $child = Situation2Vertex::create();

        $exception = new class extends LaravelDagModelException {};
        $counter = 0;
        Event::listen(
            'eloquent.saving: ' . Situation2Edge::class,
            function (Situation2Edge $edge) use (&$counter, $parent, $child, $exception) {
                $this->assertSame($parent->id, $edge->start_vertex);
                $this->assertSame($child->id, $edge->end_vertex);
                $counter++;
                throw new $exception();
            }
        );

        $thrown = false;
        try {
            $parent->children()->attach($child, ['dummy_edge_column' => 'foo']);
        } catch (LaravelDagModelException $e) {
            if (get_class($e) === get_class($exception)) {
                $thrown = true;
            } else {
                throw $e;
            }
        }

        $this->assertTrue($thrown);

        $this->assertSame(1, $counter);
        $this->assertCount(0, $parent->children()->get());
        $this->assertCount(0, $child->parents()->get());
    }

    public function testCanUseDeleteEvent()
    {
        $parent = Situation2Vertex::create();
        $child = Situation2Vertex::create();

        $parent->children()->attach($child);

        $counter = 0;
        Event::listen(
            'eloquent.deleting: ' . Situation2Edge::class,
            function (Situation2Edge $edge) use (&$counter, $parent, $child) {
                $this->assertSame($parent->id, $edge->start_vertex);
                $this->assertSame($child->id, $edge->end_vertex);
                $counter++;
            }
        );

        $parent->children()->detach($child);

        $this->assertSame(1, $counter);
        $this->assertCount(0, $parent->children()->get());
        $this->assertCount(0, $child->parents()->get());
    }

    public function testCanCancelDeleteUsingEvent()
    {
        $parent = Situation2Vertex::create();
        $child = Situation2Vertex::create();

        $parent->children()->attach($child);

        $counter = 0;
        Event::listen(
            'eloquent.deleting: ' . Situation2Edge::class,
            function (Situation2Edge $edge) use (&$counter, $parent, $child) {
                $this->assertSame($parent->id, $edge->start_vertex);
                $this->assertSame($child->id, $edge->end_vertex);
                $counter++;
                return false;
            }
        );

        $parent->children()->detach($child);

        $this->assertSame(1, $counter);
        $this->assertCount(1, $parent->children()->get());
        $this->assertCount(1, $child->parents()->get());
    }

    public function testExceptionInDeletedEventRollsBackDeletion()
    {
        $parent = Situation2Vertex::create();
        $child = Situation2Vertex::create();

        $parent->children()->attach($child);

        $exception = new class extends LaravelDagModelException {};
        $thrown = false;
        Event::listen(
            'eloquent.deleted: ' . Situation2Edge::class,
            function (Situation2Edge $edge) use ($parent, $child, $exception) {
                $this->assertCount(0, $parent->children()->get());
                $this->assertCount(0, $child->parents()->get());
                throw new $exception();
            }
        );

        try {
            $parent->children()->detach($child);
        } catch (LaravelDagModelException $e) {
            if (! $e instanceof $exception) {
                throw $e;
            } else {
                $thrown = true;
            }
        }

        $this->assertTrue($thrown);
        $this->assertCount(1, $parent->children()->get());
        $this->assertCount(1, $child->parents()->get());
    }
}
