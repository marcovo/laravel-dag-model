<?php

namespace Marcovo\LaravelDagModel\Tests\Models;

use Marcovo\LaravelDagModel\Tests\fixtures\Situation1Vertex;
use Marcovo\LaravelDagModel\Tests\TestCase;

class TraverseTest extends TestCase
{
    /**
     * @return Situation1Vertex[]
     */
    private function createChain(): array
    {
        $A = Situation1Vertex::create();
        $B = Situation1Vertex::create();
        $C = Situation1Vertex::create();
        $D = Situation1Vertex::create();

        $A->children()->attach($B);
        $B->children()->attach($C);
        $C->children()->attach($D);

        return [$A, $B, $C, $D];
    }

    public function testCanTraverseDfsPreChain()
    {
        [$A, $B, $C, $D] = $this->createChain();

        $visited = [];

        $result = $A->children()->traverseDfs(function (Situation1Vertex $m) use (&$visited) {
            $visited[] = $m->id;
        });

        $this->assertSame([
            $A->id,
            $B->id,
            $C->id,
            $D->id,
        ], $visited);

        $this->assertNull($result);
    }

    public function testCanTraverseDfsPostChain()
    {
        [$A, $B, $C, $D] = $this->createChain();

        $visited = [];

        $result = $A->children()->traverseDfsPostOrdering(function (Situation1Vertex $m) use (&$visited) {
            $visited[] = $m->id;
        });

        $this->assertSame([
            $D->id,
            $C->id,
            $B->id,
            $A->id,
        ], $visited);

        $this->assertNull($result);
    }

    public function testCanTraverseBfsChain()
    {
        [$A, $B, $C, $D] = $this->createChain();

        $visited = [];

        $result = $A->children()->traverseBfs(function (Situation1Vertex $m) use (&$visited) {
            $visited[] = $m->id;
        });

        $this->assertSame([
            $A->id,
            $B->id,
            $C->id,
            $D->id,
        ], $visited);

        $this->assertNull($result);
    }

    public function testCanTraverseAndReturnDfsPreChain()
    {
        [$A, $B, $C, $D] = $this->createChain();

        $visited = [];

        $result = $A->children()->traverseDfs(function (Situation1Vertex $m) use (&$visited, $B) {
            $visited[] = $m->id;

            if ($m->id === $B->id) {
                return 'foo';
            }
        });

        $this->assertSame([
            $A->id,
            $B->id,
        ], $visited);

        $this->assertSame('foo', $result);
    }

    public function testCanTraverseAndReturnDfsPostChain()
    {
        [$A, $B, $C, $D] = $this->createChain();

        $visited = [];

        $result = $A->children()->traverseDfsPostOrdering(function (Situation1Vertex $m) use (&$visited, $C) {
            $visited[] = $m->id;

            if ($m->id === $C->id) {
                return 'foo';
            }
        });

        $this->assertSame([
            $D->id,
            $C->id,
        ], $visited);

        $this->assertSame('foo', $result);
    }

    public function testCanTraverseAndReturnBfsChain()
    {
        [$A, $B, $C, $D] = $this->createChain();

        $visited = [];

        $result = $A->children()->traverseBfs(function (Situation1Vertex $m) use (&$visited, $B) {
            $visited[] = $m->id;

            if ($m->id === $B->id) {
                return 'foo';
            }
        });

        $this->assertSame([
            $A->id,
            $B->id,
        ], $visited);

        $this->assertSame('foo', $result);
    }

    /**
     *     A   B
     *      \ / \
     *       C   D
     *      / \ /
     *     E   F
     *    / \ / \
     *   G   H  |
     *    \ / \ |
     *     I   J
     * @return Situation1Vertex[]
     */
    private function createGraph(): array
    {
        $A = Situation1Vertex::create();
        $B = Situation1Vertex::create();
        $C = Situation1Vertex::create();
        $D = Situation1Vertex::create();
        $E = Situation1Vertex::create();
        $F = Situation1Vertex::create();
        $G = Situation1Vertex::create();
        $H = Situation1Vertex::create();
        $I = Situation1Vertex::create();
        $J = Situation1Vertex::create();

        $A->children()->attach($C);
        $B->children()->attach($C);
        $B->children()->attach($D);
        $C->children()->attach($E);
        $C->children()->attach($F);
        $D->children()->attach($F);
        $E->children()->attach($G);
        $E->children()->attach($H);
        $F->children()->attach($H);
        $G->children()->attach($I);
        $H->children()->attach($I);
        $H->children()->attach($J);
        $F->children()->attach($J);

        $C->linkDescendantChildren();
        foreach([$C, $E, $F, $H] as $vertex) {
            $vertex = $vertex->is($C) ? $C : $C->descendants->find($vertex->id);
            $vertex->children = $vertex->children->sortBy('id');
        }

        return [$A, $B, $C, $D, $E, $F, $G, $H, $I, $J];
    }

    public function testCanTraverseDfsPreGraph()
    {
        [$A, $B, $C, $D, $E, $F, $G, $H, $I, $J] = $this->createGraph();

        $visited = [];

        $C->children()->traverseDfs(function (Situation1Vertex $m) use (&$visited) {
            $visited[] = $m->id;
        });

        $this->assertSame([
            $C->id,
            $E->id,
            $G->id,
            $I->id,
            $H->id,
            $J->id,
            $F->id,
        ], $visited);
    }

    public function testCanTraverseDfsPostGraph()
    {
        [$A, $B, $C, $D, $E, $F, $G, $H, $I, $J] = $this->createGraph();

        $visited = [];

        $C->children()->traverseDfsPostOrdering(function (Situation1Vertex $m) use (&$visited) {
            $visited[] = $m->id;
        });

        $this->assertSame([
            $I->id,
            $G->id,
            $J->id,
            $H->id,
            $E->id,
            $F->id,
            $C->id,
        ], $visited);
    }

    public function testCanTraverseBfsGraph()
    {
        [$A, $B, $C, $D, $E, $F, $G, $H, $I, $J] = $this->createGraph();

        $visited = [];

        $C->children()->traverseBfs(function (Situation1Vertex $m) use (&$visited) {
            $visited[] = $m->id;
        });

        $this->assertSame([
            $C->id,
            $E->id,
            $F->id,
            $G->id,
            $H->id,
            $J->id,
            $I->id,
        ], $visited);
    }
}
