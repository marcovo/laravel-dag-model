<?php

namespace Marcovo\LaravelDagModel\Tests\Models;

use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation1Vertex;
use Marcovo\LaravelDagModel\Tests\TestCase;

class BelongsToManyInTransitiveClosureTest extends TestCase
{
    // TODO: Uncomment whenever we drop support for laravel < 9
    // public function testCannotCallFirstOrCreate()
    // {
    //     $vertex = Situation1Vertex::create();
    //
    //     $this->expectException(MethodNotSupportedException::class);
    //     $vertex->ancestors()->firstOrCreate([]);
    // }

    public function testCannotCallUpdateOrCreate()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->updateOrCreate([]);
    }

    public function testCannotCallSave()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->save(Situation1Vertex::make());
    }

    public function testCannotCallSaveMany()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->saveMany([]);
    }

    public function testCannotCallCreate()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->create();
    }

    public function testCannotCallCreateMany()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->createMany([]);
    }

    public function testCannotCallToggle()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->toggle([]);
    }

    public function testCannotCallSync()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->sync([]);
    }

    public function testCannotCallUpdateExistingPivotWithManagedColumns()
    {
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();

        $parent->children()->attach($child);

        $this->expectException(MethodNotSupportedException::class);
        $parent->descendants()->updateExistingPivot($child->id, ['start_vertex' => 3]);
    }

    public function testCallUpdateExistingPivotWithUnmanagedColumns()
    {
        $parent = Situation1Vertex::create();
        $child = Situation1Vertex::create();

        $parent->children()->attach($child);

        $parent->descendants()->updateExistingPivot($child, ['created_at' => '2022-01-01 00:00:00']);
        $this->assertSame('2022-01-01 00:00:00', (string)$parent->descendants()->withPivot(['created_at'])->find($child->id)->pivot->created_at);

        $parent->descendants()->updateExistingPivot($child, ['created_at' => '2022-01-31 10:00:00']);
        $this->assertSame('2022-01-31 10:00:00', (string)$parent->descendants()->withPivot(['created_at'])->find($child->id)->pivot->created_at);
    }

    public function testCannotCallAttach()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->attach([]);
    }

    public function testCannotCallDetach()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->detach([]);
    }

    public function testCannotCallInsert()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->insert([]);
    }

    public function testCannotCallInsertOrIgnore()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->insertOrIgnore([]);
    }

    public function testCannotCallInsertGetId()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->insertGetId([]);
    }

    public function testCannotCallInsertUsing()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->insertUsing([], '');
    }

    public function testCannotCallUpdateOrInsert()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->updateOrInsert([]);
    }

    public function testCannotCallDelete()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->delete();
    }

    public function testCannotCallForceDelete()
    {
        $vertex = Situation1Vertex::create();

        $this->expectException(MethodNotSupportedException::class);
        $vertex->ancestors()->forceDelete();
    }
}
