<?php

namespace Marcovo\LaravelDagModel\Tests\Models;

use Marcovo\LaravelDagModel\Exceptions\SeparationException;
use Marcovo\LaravelDagModel\Tests\fixtures\Situation4Vertex;
use Marcovo\LaravelDagModel\Tests\TestCase;

class SeparationTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Situation4Vertex::unguard();
    }

    public function testCanAttachParent()
    {
        $child1 = Situation4Vertex::create(['separation_column' => 'foo']);
        $parent1 = Situation4Vertex::create(['separation_column' => 'foo']);

        $child2 = Situation4Vertex::create(['separation_column' => 'bar']);
        $parent2 = Situation4Vertex::create(['separation_column' => 'bar']);

        $child1->parents()->attach($parent1);
        $child2->parents()->attach($parent2);

        $this->assertCount(1, $child1->parents);
        $this->assertTrue($parent1->is($child1->parents->first()));

        $this->assertCount(1, $child2->parents);
        $this->assertTrue($parent2->is($child2->parents->first()));
    }

    public function testCannotAttachParentFromOtherDag()
    {
        $child1 = Situation4Vertex::create(['separation_column' => 'foo']);
        $parent2 = Situation4Vertex::create(['separation_column' => 'bar']);

        $this->expectException(SeparationException::class);
        $child1->parents()->attach($parent2);
    }

    public function testCanGetParents()
    {
        $child1 = Situation4Vertex::create(['separation_column' => 'foo']);
        $parent1 = Situation4Vertex::create(['separation_column' => 'foo']);

        $child2 = Situation4Vertex::create(['separation_column' => 'bar']);
        $parent2 = Situation4Vertex::create(['separation_column' => 'bar']);

        $parent1->children()->attach($child1);
        $parent2->children()->attach($child2);

        $this->assertTrue($parent1->is($child1->parents()->first()));
        $this->assertTrue($parent2->is($child2->parents()->first()));
    }

    public function testCanGetAncestors()
    {
        $grandparent1 = Situation4Vertex::create(['separation_column' => 'foo']);
        $parent1 = Situation4Vertex::create(['separation_column' => 'foo']);
        $child1 = Situation4Vertex::create(['separation_column' => 'foo']);

        $grandparent2 = Situation4Vertex::create(['separation_column' => 'bar']);
        $parent2 = Situation4Vertex::create(['separation_column' => 'bar']);
        $child2 = Situation4Vertex::create(['separation_column' => 'bar']);

        $grandparent1->children()->attach($parent1);
        $parent1->children()->attach($child1);

        $grandparent2->children()->attach($parent2);
        $parent2->children()->attach($child2);

        $ancestors1 = $child1->ancestors()->orderBy('id')->get();
        $this->assertTrue($grandparent1->is($ancestors1[0]));
        $this->assertTrue($parent1->is($ancestors1[1]));

        $ancestors2 = $child2->ancestors()->orderBy('id')->get();
        $this->assertTrue($grandparent2->is($ancestors2[0]));
        $this->assertTrue($parent2->is($ancestors2[1]));
    }
}
