<?php

namespace Marcovo\LaravelDagModel\Tests;

use Marcovo\LaravelDagModel\Exceptions\CycleException;
use Marcovo\LaravelDagModel\Exceptions\DuplicateEdgeException;
use Marcovo\LaravelDagModel\Exceptions\EdgeNotFoundException;
use Marcovo\LaravelDagModel\Exceptions\InDegreeException;
use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;
use Marcovo\LaravelDagModel\Exceptions\OutDegreeException;
use Marcovo\LaravelDagModel\Exceptions\PotentialPathCountOverflowException;
use Marcovo\LaravelDagModel\Exceptions\SelfLinkException;
use Marcovo\LaravelDagModel\Exceptions\SeparationException;
use Marcovo\LaravelDagModel\Exceptions\UnsupportedDatabaseDriverException;

class ExceptionTest extends TestCase
{
    public function testCycleException()
    {
        $exception = new CycleException();
        $this->assertSame('The requested edge would create a directed cycle in the graph', $exception->getMessage());
    }

    public function testDuplicateEdgeException()
    {
        $exception = new DuplicateEdgeException();
        $this->assertSame('The requested edge already exists in graph', $exception->getMessage());
    }

    public function testEdgeNotFoundException()
    {
        $exception = new EdgeNotFoundException();
        $this->assertSame('Could not find edge in the graph', $exception->getMessage());
    }

    public function testSelfLinkException()
    {
        $exception = new SelfLinkException();
        $this->assertSame('Cannot create an edge from a vertex to itself', $exception->getMessage());
    }

    public function testUnsupportedDatabaseDriverException()
    {
        $exception = new UnsupportedDatabaseDriverException();
        $this->assertSame('Unsupported database driver', $exception->getMessage());
    }

    public function testPotentialPathCountOverflowException()
    {
        $exception = new PotentialPathCountOverflowException();
        $this->assertStringContainsString('Adding new edges can potentially overflow the patch count register.', $exception->getMessage());
    }

    public function testMethodNotSupportedException()
    {
        $exception = new MethodNotSupportedException();
        $this->assertSame('This method call is not supported', $exception->getMessage());
    }

    public function testSeparationException()
    {
        $exception = new SeparationException();
        $this->assertSame('Could not perform action due to vertices being separated', $exception->getMessage());
    }

    public function testInDegreeException()
    {
        $exception = InDegreeException::make(3);
        $this->assertSame('Vertex already has 3 parents', $exception->getMessage());
    }

    public function testOutDegreeException()
    {
        $exception = OutDegreeException::make(3);
        $this->assertSame('Vertex already has 3 children', $exception->getMessage());
    }
}
