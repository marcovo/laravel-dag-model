<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\ForestAlgorithm;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationForestAlgorithmEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\ForestAlgorithm;

class RebuildTest extends TestCase
{
    protected function getAlgorithm(): ForestAlgorithm
    {
        return new ForestAlgorithm(new SituationForestAlgorithmEdge());
    }

    private function getAllEdges(): Collection
    {
        return DB::table('situation_forest_algorithm_edge')->get();
    }

    /**
     * Tests rebuilding a mutated tree
     *       1
     *      / \
     *     2   3
     *    / \ / \
     *   4  5 6  7
     */
    public function test_can_rebuild_mutated_tree()
    {
        $this->withVertices($V = [1, 2, 3, 4, 5, 6, 7]);

        $this->assertEdges([], [], $V, $this->getAllEdges());

        $vertices = [
            [1, 2],
            [1, 3],
            [2, 4],
            [2, 5],
            [3, 6],
            [3, 7],
        ];

        for ($i = 0; $i < 6; $i++) {
            $this->getAlgorithm()->createEdge($vertices[$i][0], $vertices[$i][1]);
        }

        // Add erroneous TC edges
        foreach ([[2, 6], [5, 7]] as [$start, $end]) {
            DB::table('situation_forest_algorithm_edge')->insert([
                'start_vertex' => $start,
                'end_vertex' => $end,
                'edge_type' => IsEdgeInDagContract::TYPE_CLOSURE_EDGE,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        // Remove existing TC edge
        DB::table('situation_forest_algorithm_edge')
            ->where('start_vertex', '=', 1)
            ->where('end_vertex', '=', 5)
            ->delete();

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
                [2, 4],
                [2, 5],
                [3, 6],
                [3, 7],
            ],
            [
                [1, 4],
                //[1, 5],
                [1, 6],
                [1, 7],
                [2, 6], // !
                [5, 7], // !
            ],
            $V,
            $this->getAllEdges()
        );

        $this->getAlgorithm()->rebuild();

        $this->assertEdges(
            [
                [1, 2],
                [1, 3],
                [2, 4],
                [2, 5],
                [3, 6],
                [3, 7],
            ],
            [
                [1, 4],
                [1, 5],
                [1, 6],
                [1, 7],
            ],
            $V,
            $this->getAllEdges()
        );
    }
}
