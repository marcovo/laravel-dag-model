<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\ForestAlgorithm;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Exceptions\DuplicateEdgeException;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationForestAlgorithmStringEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\ForestAlgorithm;

class StringKeyTest extends TestCase
{
    private function getForestAlgorithm(): ForestAlgorithm
    {
        return new ForestAlgorithm(new SituationForestAlgorithmStringEdge());
    }

    private function getAllEdges(): Collection
    {
        return DB::table('situation_forest_algorithm_string_edge')->get();
    }

    /**
     * Create Tree
     *      A
     *     / \
     *    B   C
     *   / \ / \
     *  D  E F  G
     */
    private function createTree()
    {
        $this->getForestAlgorithm()->createEdge('A', 'B');
        $this->getForestAlgorithm()->createEdge('A', 'C');

        $this->getForestAlgorithm()->createEdge('B', 'D');
        $this->getForestAlgorithm()->createEdge('B', 'E');
        $this->getForestAlgorithm()->createEdge('C', 'F');
        $this->getForestAlgorithm()->createEdge('C', 'G');
    }

    public function test_can_create_tree_with_string_vertices()
    {
        $this->createTree();

        $this->assertEdges(
            [
                ['A', 'B'],
                ['A', 'C'],
                ['B', 'D'],
                ['B', 'E'],
                ['C', 'F'],
                ['C', 'G'],
            ],
            [
                ['A', 'D'],
                ['A', 'E'],
                ['A', 'F'],
                ['A', 'G'],
            ],
            [],
            $this->getAllEdges()
        );
    }

    public function test_can_delete_edge()
    {
        $this->createTree();

        $this->getForestAlgorithm()->deleteEdge('C', 'F');

        $this->assertEdges(
            [
                ['A', 'B'],
                ['A', 'C'],
                ['B', 'D'],
                ['B', 'E'],
                ['C', 'G'],
            ],
            [
                ['A', 'D'],
                ['A', 'E'],
                ['A', 'G'],
            ],
            [],
            $this->getAllEdges()
        );

        $this->getForestAlgorithm()->deleteEdge('A', 'B');

        $this->assertEdges(
            [
                ['A', 'C'],
                ['B', 'D'],
                ['B', 'E'],
                ['C', 'G'],
            ],
            [
                ['A', 'G'],
            ],
            [],
            $this->getAllEdges()
        );
    }

    public function test_cannot_add_edge_twice()
    {
        $this->getForestAlgorithm()->createEdge('A', 'B');

        $this->expectException(DuplicateEdgeException::class);
        $this->getForestAlgorithm()->createEdge('A', 'B');
    }

    public function test_can_determine_has_a_graph_edge()
    {
        $this->getForestAlgorithm()->createEdge('A', 'B');

        $this->assertTrue($this->getForestAlgorithm()->hasGraphEdge('A', 'B'));
        $this->assertTrue($this->getForestAlgorithm()->hasEdge('A', 'B'));
    }

    public function test_can_determine_has_a_transitive_closure_edge()
    {
        $this->getForestAlgorithm()->createEdge('A', 'B');
        $this->getForestAlgorithm()->createEdge('B', 'C');

        $this->assertFalse($this->getForestAlgorithm()->hasGraphEdge('A', 'C'));
        $this->assertTrue($this->getForestAlgorithm()->hasEdge('A', 'C'));
    }

    public function test_can_get_a_graph_edge()
    {
        $this->getForestAlgorithm()->createEdge('A', 'B');

        $this->assertNotNull($edge1 = $this->getForestAlgorithm()->getGraphEdge('A', 'B'));
        $this->assertSame('A', $edge1->start_vertex);
        $this->assertSame('B', $edge1->end_vertex);

        $this->assertNotNull($edge2 = $this->getForestAlgorithm()->getEdge('A', 'B'));
        $this->assertSame('A', $edge2->start_vertex);
        $this->assertSame('B', $edge2->end_vertex);
    }

    public function test_can_get_a_transitive_closure_edge()
    {
        $this->getForestAlgorithm()->createEdge('A', 'B');
        $this->getForestAlgorithm()->createEdge('B', 'C');

        $this->assertNull($this->getForestAlgorithm()->getGraphEdge('A', 'C'));
        $this->assertNotNull($edge = $this->getForestAlgorithm()->getEdge('A', 'C'));
        $this->assertSame('A', $edge->start_vertex);
        $this->assertSame('C', $edge->end_vertex);
    }

    public function test_can_rebuild_mutated_tree()
    {
        $this->createTree();

        // Add erroneous TC edges
        foreach ([['B', 'F'], ['E', 'G']] as [$start, $end]) {
            DB::table('situation_forest_algorithm_string_edge')->insert([
                'start_vertex' => $start,
                'end_vertex' => $end,
                'edge_type' => IsEdgeInDagContract::TYPE_CLOSURE_EDGE,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        // Remove existing TC edge
        DB::table('situation_forest_algorithm_string_edge')
            ->where('start_vertex', '=', 'A')
            ->where('end_vertex', '=', 'E')
            ->delete();

        $this->assertEdges(
            [
                ['A', 'B'],
                ['A', 'C'],
                ['B', 'D'],
                ['B', 'E'],
                ['C', 'F'],
                ['C', 'G'],
            ],
            [
                ['A', 'D'],
                //['A', 'E'],
                ['A', 'F'],
                ['A', 'G'],
                ['B', 'F'], // !
                ['E', 'G'], // !
            ],
            [],
            $this->getAllEdges()
        );

        $this->getForestAlgorithm()->rebuild();

        $this->assertEdges(
            [
                ['A', 'B'],
                ['A', 'C'],
                ['B', 'D'],
                ['B', 'E'],
                ['C', 'F'],
                ['C', 'G'],
            ],
            [
                ['A', 'D'],
                ['A', 'E'],
                ['A', 'F'],
                ['A', 'G'],
            ],
            [],
            $this->getAllEdges()
        );
    }
}
