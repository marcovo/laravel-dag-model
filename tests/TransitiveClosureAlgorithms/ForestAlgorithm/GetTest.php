<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\ForestAlgorithm;

use Marcovo\LaravelDagModel\Tests\fixtures\SituationForestAlgorithmEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\ForestAlgorithm;

class GetTest extends TestCase
{
    protected function getAlgorithm(): ForestAlgorithm
    {
        return new ForestAlgorithm(new SituationForestAlgorithmEdge());
    }

    public function test_can_construct_forest_algorithm()
    {
        $forestAlgorithm = $this->getAlgorithm();

        $this->assertSame(['start_vertex', 'end_vertex', 'edge_type'], $forestAlgorithm->getManagedColumns());
        $this->assertSame(['edge_type'], $forestAlgorithm->getAlgorithmSpecificColumns());
    }

    public function test_can_determine_has_a_graph_edge()
    {
        $this->withVertices([1, 2]);

        $this->assertFalse($this->getAlgorithm()->hasGraphEdge(1, 2));
        $this->assertFalse($this->getAlgorithm()->hasEdge(1, 2));

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertTrue($this->getAlgorithm()->hasGraphEdge(1, 2));
        $this->assertTrue($this->getAlgorithm()->hasEdge(1, 2));
    }

    public function test_can_determine_has_a_transitive_closure_edge()
    {
        $this->withVertices([1, 2, 3]);

        $this->assertFalse($this->getAlgorithm()->hasGraphEdge(1, 3));
        $this->assertFalse($this->getAlgorithm()->hasEdge(1, 3));

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertFalse($this->getAlgorithm()->hasGraphEdge(1, 3));
        $this->assertTrue($this->getAlgorithm()->hasEdge(1, 3));
    }

    public function test_can_get_a_graph_edge()
    {
        $this->withVertices([1, 2]);

        $this->assertNull($this->getAlgorithm()->getGraphEdge(1, 2));
        $this->assertNull($this->getAlgorithm()->getEdge(1, 2));

        $this->getAlgorithm()->createEdge(1, 2);

        $this->assertNotNull($edge1 = $this->getAlgorithm()->getGraphEdge(1, 2));
        $this->assertSame(1, (int)$edge1->start_vertex);
        $this->assertSame(2, (int)$edge1->end_vertex);

        $this->assertNotNull($edge2 = $this->getAlgorithm()->getEdge(1, 2));
        $this->assertSame(1, (int)$edge2->start_vertex);
        $this->assertSame(2, (int)$edge2->end_vertex);
    }

    public function test_can_get_a_transitive_closure_edge()
    {
        $this->withVertices([1, 2, 3]);

        $this->assertNull($this->getAlgorithm()->getGraphEdge(1, 3));
        $this->assertNull($this->getAlgorithm()->getEdge(1, 3));

        $this->getAlgorithm()->createEdge(1, 2);
        $this->getAlgorithm()->createEdge(2, 3);

        $this->assertNull($this->getAlgorithm()->getGraphEdge(1, 3));
        $this->assertNotNull($edge = $this->getAlgorithm()->getEdge(1, 3));
        $this->assertSame(1, (int)$edge->start_vertex);
        $this->assertSame(3, (int)$edge->end_vertex);
    }

    public function test_has_no_edge_to_itself()
    {
        $this->withVertices([1]);

        $this->assertFalse($this->getAlgorithm()->hasGraphEdge(1, 1));
        $this->assertFalse($this->getAlgorithm()->hasEdge(1, 1));
    }
}
