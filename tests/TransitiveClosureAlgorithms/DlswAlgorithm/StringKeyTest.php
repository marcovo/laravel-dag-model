<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\DlswAlgorithm;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Tests\fixtures\SituationDlswAlgorithmStringEdge;
use Marcovo\LaravelDagModel\Tests\TestCase;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\DlswAlgorithm;

class StringKeyTest extends TestCase
{
    private function getDlsw(): DlswAlgorithm
    {
        return new DlswAlgorithm(new SituationDlswAlgorithmStringEdge());
    }

    private function getAllEdges(): Collection
    {
        return DB::table('situation_dlsw_algorithm_string_edge')->get();
    }

    /**
     * Create diagonal diamond
     *       A
     *      / \
     *     B   C
     *     | \ |
     *     D   E
     *      \ /
     *       F
     */
    private function createDiagonalDiamond()
    {
        $vertices = [
            ['A', 'B'],
            ['A', 'C'],
            ['B', 'D'],
            ['B', 'E'],
            ['C', 'E'],
            ['D', 'F'],
            ['E', 'F'],
        ];

        for ($i = 0; $i < 7; $i++) {
            $this->getDlsw()->createEdge($vertices[$i][0], $vertices[$i][1]);
        }
    }

    public function test_can_create_graph_with_string_vertices()
    {
        $this->createDiagonalDiamond();

        $this->assertEdges(
            [
                ['A', 'B'],
                ['A', 'C'],
                ['B', 'D'],
                ['B', 'E'],
                ['C', 'E'],
                ['D', 'F'],
                ['E', 'F'],
            ],
            [
                ['A', 'D'],
                ['B', 'F'],
                ['A', 'E'],
                ['C', 'F'],
                ['A', 'F'],
            ],
            [],
            $this->getAllEdges()
        );
    }

    public function test_can_delete_edge()
    {
        $this->createDiagonalDiamond();

        $this->getDlsw()->deleteEdge('C', 'E');

        $this->assertEdges(
            [
                ['A', 'B'],
                ['A', 'C'],
                ['B', 'D'],
                ['B', 'E'],
                ['D', 'F'],
                ['E', 'F'],
            ],
            [
                ['A', 'D'],
                ['B', 'F'],
                ['A', 'E'],
                ['A', 'F'],
            ],
            [],
            $this->getAllEdges()
        );

        $this->getDlsw()->deleteEdge('B', 'D');

        $this->assertEdges(
            [
                ['A', 'B'],
                ['A', 'C'],
                ['B', 'E'],
                ['D', 'F'],
                ['E', 'F'],
            ],
            [
                ['B', 'F'],
                ['A', 'E'],
                ['A', 'F'],
            ],
            [],
            $this->getAllEdges()
        );
    }

    public function test_can_determine_has_a_graph_edge()
    {
        $this->getDlsw()->createEdge('A', 'B');

        $this->assertTrue($this->getDlsw()->hasGraphEdge('A', 'B'));
        $this->assertTrue($this->getDlsw()->hasEdge('A', 'B'));
    }

    public function test_can_determine_has_a_transitive_closure_edge()
    {
        $this->getDlsw()->createEdge('A', 'B');
        $this->getDlsw()->createEdge('B', 'C');

        $this->assertFalse($this->getDlsw()->hasGraphEdge('A', 'C'));
        $this->assertTrue($this->getDlsw()->hasEdge('A', 'C'));
    }

    public function test_can_get_a_graph_edge()
    {
        $this->getDlsw()->createEdge('A', 'B');

        $this->assertNotNull($edge1 = $this->getDlsw()->getGraphEdge('A', 'B'));
        $this->assertSame('A', $edge1->start_vertex);
        $this->assertSame('B', $edge1->end_vertex);

        $this->assertNotNull($edge2 = $this->getDlsw()->getEdge('A', 'B'));
        $this->assertSame('A', $edge2->start_vertex);
        $this->assertSame('B', $edge2->end_vertex);
    }

    public function test_can_get_a_transitive_closure_edge()
    {
        $this->getDlsw()->createEdge('A', 'B');
        $this->getDlsw()->createEdge('B', 'C');

        $this->assertNull($this->getDlsw()->getGraphEdge('A', 'C'));
        $this->assertNotNull($edge = $this->getDlsw()->getEdge('A', 'C'));
        $this->assertSame('A', $edge->start_vertex);
        $this->assertSame('C', $edge->end_vertex);
    }

    public function test_can_rebuild_mutated_diagonal_diamond()
    {
        $this->createDiagonalDiamond();

        // Add erroneous TC edges
        foreach ([['C', 'D'], ['E', 'G'], ['G', 'H']] as [$start, $end]) {
            DB::table('situation_dlsw_algorithm_string_edge')->insert([
                'start_vertex' => $start,
                'end_vertex' => $end,
                'edge_type' => IsEdgeInDagContract::TYPE_CLOSURE_EDGE,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        // Remove existing TC edge
        DB::table('situation_dlsw_algorithm_string_edge')
            ->where('start_vertex', '=', 'A')
            ->where('end_vertex', '=', 'E')
            ->delete();

        $this->assertEdges(
            [
                ['A', 'B'],
                ['A', 'C'],
                ['B', 'D'],
                ['B', 'E'],
                ['C', 'E'],
                ['D', 'F'],
                ['E', 'F'],
            ],
            [
                ['A', 'D'],
                ['B', 'F'],
                //['A', 'E'],
                ['C', 'F'],
                ['A', 'F'],
                ['C', 'D'], // !
                ['E', 'G'], // !
                ['G', 'H'], // !
            ],
            [],
            $this->getAllEdges()
        );

        $this->getDlsw()->rebuild();

        $this->assertEdges(
            [
                ['A', 'B'],
                ['A', 'C'],
                ['B', 'D'],
                ['B', 'E'],
                ['C', 'E'],
                ['D', 'F'],
                ['E', 'F'],
            ],
            [
                ['A', 'D'],
                ['B', 'F'],
                ['A', 'E'],
                ['C', 'F'],
                ['A', 'F'],
            ],
            [],
            $this->getAllEdges()
        );
    }
}
