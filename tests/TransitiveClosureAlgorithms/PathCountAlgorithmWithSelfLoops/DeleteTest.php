<?php

namespace Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\PathCountAlgorithmWithSelfLoops;

use Marcovo\LaravelDagModel\Tests\fixtures\SituationPathCountAlgorithmSelfLoopsEdge;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\PathCountAlgorithm;

class DeleteTest extends \Marcovo\LaravelDagModel\Tests\TransitiveClosureAlgorithms\PathCountAlgorithm\DeleteTest
{
    protected bool $checkSelfLoops = true;

    protected function getAlgorithm(): PathCountAlgorithm
    {
        return new PathCountAlgorithm(new SituationPathCountAlgorithmSelfLoopsEdge());
    }
}
