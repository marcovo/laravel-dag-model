<?php

namespace Marcovo\LaravelDagModel\Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;

abstract class BaseTestCase extends \Orchestra\Testbench\TestCase
{
    // This file exists purely to be able to use:
    //  - refreshInMemoryDatabase() up to Laravel 10
    //  - restoreInMemoryDatabase() in Laravel 11
    // as giving them both a `use` alias is not possible due to one of the always missing
    use RefreshDatabase;
}
