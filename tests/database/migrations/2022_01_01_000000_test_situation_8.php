<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TestSituation8 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('situation_8_vertex', function (Blueprint $table) {
            $table->string('uuid')->primary();
            $table->timestamps();
        });

        Schema::create('situation_8_edge', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('start_vertex');
            $table->string('end_vertex');
            $table->unsignedBigInteger('path_count');
            $table->unsignedTinyInteger('edge_type');
            $table->timestamps();
            $table->unique(['start_vertex', 'end_vertex']);
            $table->index(['end_vertex']);

            $table->foreign('start_vertex', 'situation8_start_vertex')->on('situation_8_vertex')->references('uuid')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('end_vertex', 'situation8_end_vertex')->on('situation_8_vertex')->references('uuid')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('situation_8_edge');

        Schema::dropIfExists('situation_8_vertex');
    }
}
