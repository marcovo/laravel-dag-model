<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TestSituationForestAlgorithm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('situation_forest_algorithm_edge', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('start_vertex');
            $table->unsignedBigInteger('end_vertex');
            $table->unsignedTinyInteger('edge_type');
            $table->timestamps();
            $table->unique(['start_vertex', 'end_vertex']);
            $table->index(['end_vertex']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('situation_forest_algorithm_edge');
    }
}
