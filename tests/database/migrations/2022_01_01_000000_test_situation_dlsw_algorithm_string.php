<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TestSituationDlswAlgorithmString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('situation_dlsw_algorithm_string_edge', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('start_vertex');
            $table->string('end_vertex');
            $table->unsignedTinyInteger('edge_type');
            $table->timestamps();
            $table->unique(['start_vertex', 'end_vertex'], 'sit_dlsw_edge_start_end_unique_str');
            $table->index(['end_vertex']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('situation_dlsw_algorithm_string_edge');
    }
}
