<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TestSituation5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('situation_5_vertex', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('top_order');
            $table->timestamps();
        });

        Schema::create('situation_5_edge', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('start_vertex');
            $table->unsignedBigInteger('end_vertex');
            $table->unsignedBigInteger('path_count');
            $table->unsignedTinyInteger('edge_type');
            $table->timestamps();
            $table->unique(['start_vertex', 'end_vertex']);
            $table->index(['end_vertex']);

            $table->foreign('start_vertex', 'situation5_start_vertex')->on('situation_5_vertex')->references('id')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('end_vertex', 'situation5_end_vertex')->on('situation_5_vertex')->references('id')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('situation_5_edge');

        Schema::dropIfExists('situation_5_vertex');
    }
}
