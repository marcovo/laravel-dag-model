#!/usr/bin/env php
<?php

if ($argc == 0) {
    die("Usage: php TestRunner.php [phpunit config]");
}

ini_set('memory_limit', '512M'); // gitlab-runner needs more than 128M

$baseDir = realpath(__DIR__ . '/../').'/';

require $baseDir . 'vendor/autoload.php';

$coverageDir = 'coverage/';
if (!is_dir($baseDir.$coverageDir)) {
    mkdir('./'.$coverageDir, 0755, true);
}

$junitDir = 'junit/';
if (!is_dir($baseDir.$junitDir)) {
    mkdir($baseDir.$junitDir, 0755, true);
}
if (!is_dir($baseDir.$junitDir.'intermediate/')) {
    mkdir($baseDir.$junitDir.'intermediate/', 0755, true);
}

$phpunitVersion = explode(' ', explode('.', shell_exec('vendor/bin/phpunit --version'))[0])[1];
$definition = $phpunitVersion >= 10 ? 10 : 9;

$configFiles = [];
$coverageHtml = $coverageClover = $coverageCobertura = $logJunit = false;
$nativeOptions = [];

for ($i=1; $i<$argc; $i++) {
    $arg = $argv[$i];

    if ($arg == '--coverage-html') {
        $coverageHtml = true;
        continue;
    }

    if ($arg == '--coverage-clover') {
        $coverageClover = true;
        continue;
    }

    if ($arg == '--coverage-cobertura') {
        $coverageCobertura = true;
        continue;
    }

    if ($arg == '--log-junit') {
        $logJunit = true;
        continue;
    }

    if (is_file($baseDir.'phpunit-' . $definition . '-' . $arg . '.xml')) {
        $configFiles[] = 'phpunit-' . $definition . '-' . $arg . '.xml';
        continue;
    }

    $nativeOptions = array_slice($argv, $i);
    break;
}

$availableConfigFiles = [];
$iterator = new GlobIterator($baseDir . 'phpunit-' . $definition . '-*.xml', FilesystemIterator::KEY_AS_FILENAME);
foreach ($iterator as $item) {
    $availableConfigFiles[] = $item->getFilename();
}

if (empty($configFiles)) {
    $configFiles = $availableConfigFiles;
} else {
    $configFiles = array_unique(array_intersect($configFiles, $availableConfigFiles));
}

if (empty($configFiles)) {
    print "Empty config files list".PHP_EOL;
    die();
}

$resultCode = 0;
foreach ($configFiles as $configFile) {
    print "Running test '".$configFile."'...".PHP_EOL.PHP_EOL;

    $command = 'vendor/bin/phpunit --configuration '.$configFile;

    if($coverageHtml || $coverageClover || $coverageCobertura) {
        $command = 'XDEBUG_MODE=coverage ' . $command . ' --coverage-php '.$baseDir.$coverageDir.$configFile.'.cov';
    }

    if ($logJunit) {
        $command .= ' --log-junit '.$baseDir.$junitDir.'intermediate/'.$configFile.'.xml';
    }

    $command .= ' ' . implode(' ', $nativeOptions);

    echo $command . PHP_EOL;
    passthru($command, $return_var);

    if($return_var) {
        print "Test failed".PHP_EOL;
        $resultCode = $return_var;
    }
}

if ($coverageHtml || $coverageClover || $coverageCobertura) {
    // https://stackoverflow.com/questions/10167775/aggregating-code-coverage-from-several-executions-of-phpunit/14874892#14874892
    $codeCoverage = null;

    foreach ($configFiles as $configFile) {
        // See PHP_CodeCoverage_Report_PHP::process
        /** @var \SebastianBergmann\CodeCoverage\CodeCoverage $cov */
        $cov = require $baseDir.$coverageDir.$configFile.'.cov';
        if (isset($codeCoverage)) {
            $codeCoverage->filter()->includeFiles($cov->filter()->files());
            $codeCoverage->merge($cov);
        } else {
            $codeCoverage = $cov;
        }
    }

    $report = $codeCoverage->getReport();
    if ($report->numberOfExecutableLines() > 0) {
        $linePercentage = $report->numberOfExecutedLines() / $report->numberOfExecutableLines() * 100;
    } else {
        $linePercentage = 100.0;
    }
    printf(
        PHP_EOL . '  Total Line Coverage:   %6s (%d/%d)' . PHP_EOL,
        sprintf('%01.2F%%', $linePercentage),
        $report->numberOfExecutedLines(),
        $report->numberOfExecutableLines()
    );

    if ($coverageHtml) {
        print "\nGenerating code coverage report in HTML format ...";

        // Based on PHPUnit_TextUI_TestRunner::doRun
        $writer = new \SebastianBergmann\CodeCoverage\Report\Html\Facade();

        $writer->process($codeCoverage, $baseDir.$coverageDir);
    }

    if ($coverageClover) {
        print "\nGenerating code coverage report in clover format ...";

        $writer = new \SebastianBergmann\CodeCoverage\Report\Clover();

        $writer->process($codeCoverage, $baseDir.$coverageDir.'clover.xml');
    }

    if ($coverageCobertura) {
        print "\nGenerating code coverage report in cobertura format ...";

        $writer = new \SebastianBergmann\CodeCoverage\Report\Cobertura();

        $writer->process($codeCoverage, $baseDir.$coverageDir.'coverage.cobertura.xml');
    }

    print " done\n";
}

if ($logJunit) {
    print "Writing combined junit file ...";
    passthru('vendor/bin/phpjunitmerge ' . $baseDir.$junitDir . 'intermediate/ ' . $baseDir.$junitDir.'report.xml');
    print " done\n";
}

die($resultCode);
