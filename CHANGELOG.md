# Changelog
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
See the Versioning section in the [README](README.md#user-content-versioning) for more details.

## [Unreleased]

## [0.6.0] - 2023-03-16
### Added
- Support Laravel 11

## [0.5.0] - 2023-12-01
### Added
- PHP 8.3 support

## [0.4.1] - 2023-09-29
### Fixed
- Update signature of `HasOneParentInForest::firstOrNew()` to align with [changed signature](https://github.com/laravel/framework/pull/48542) in Laravel's `HasManyThrough::firstOrNew()`

## [0.4.0] - 2023-02-17
### Added
- Support Laravel 10

## [0.3.0] - 2022-10-16
### Added
- Relations `ancestorsWithSelf` and `descendantsWithSelf`
- `WithSelfLoops` extension

### Changed
- **[Breaking]** `boolean` database column `graph_edge` has changed to an `unsignedTinyInteger` column `edge_type`. Old value `false` should be mapped to new integer value `0`, old value `true` should be mapped to new value `1`. If the database engine has created a `unsignedTinyInteger` column for the `boolean` column, only the name change may be breaking, which could be easily solved by including the name override `public function getEdgeTypeColumn(): string { return 'graph_edge'; }`.

### Fixed
- Fixed bug where the `ForestAlgorithm`, when using a `string` primary key, would throw an `InDegreeException` instead of a `DuplicateEdgeException` when creating an already existing edge

## [0.2.0] - 2022-09-09
### Added
- PHP 8.2 support
- 4 new vertex model methods:
  - `queryIsAncestorOf($vertex): bool`
  - `queryIsDescendantOf($vertex): bool`
  - `queryIsParentOf($vertex): bool`
  - `queryIsChildOf($vertex): bool`

### Fixed
- Updated readme & docs
- Updated composer.json to use recommended formatting
- Fixed `null` reference when linking a nonexistent vertex in MySQL

## [0.1.1] - 2022-04-08
### Fixed
- Fixed badge presentation on packagist

## [0.1.0] - 2022-04-08
### Added
- Initial release
- `DlswAlgorithm`, `PathCountAlgorithm` and `ForestAlgorithm`
- Base model methods for maintaining vertices and edges
- Extensions `WithMaxVertexDegree`, `IsForest` and `WithTopologicalOrdering`
