# Public API - Topological ordering extension

- [Getting started](getting-started.md)
- [Important notes](important.md)
- API
  - [General](api-general.md)
  - [Max Vertex Degree](api-max-vertex-degree.md)
  - [Forest (trees)](api-forest.md)
  - **[Topological ordering](api-topological-ordering.md)**
    - [Description](#user-content-description)
    - [Examples](#user-content-examples)
    - [Trait `WithTopologicalOrdering`](#user-content-trait-withtopologicalordering)
  - [Self loops](api-self-loops.md)

`WithTopologicalOrdering` - *A trait for your vertex model, defining a consistent vertex ordering.*

## Description
A topological ordering defines an ordering of the vertices in the graph, such that any vertex will be ordered after all of its ancestors.
Note that a topological ordering in general is not uniquely defined: multiple topological orderings can be valid for the same graph.

***NOTE:*** *This trait requires an additional integer column on the vertex model. By default, the column name `top_order` is assumed, but this can be customized as described below.*

## Examples
Given a vertex model instance `$vertex`, you can e.g.:
```php
    // Obtain an ordered list of descendants or ancestors
    $vertex->descendantsOrdered
    $vertex->ancestorsOrdered
    $vertex->ancestorsOrderedDesc
    
    // Use the topological ordering in any other builder expression
    $query->orderTopologically('desc')
```

## Trait `WithTopologicalOrdering`

### Method `getTopologicalOrderingColumn(): string`
Override this method to provide a custom column name to store the ordering in.
This column should be an integer column.

### Scope `orderTopologically(string $direction = 'asc')`
Orders the query by the topological ordering column.
You can provide the direction, `'asc'` or `'desc'`.

### Methods `descendantsOrdered()`, `ancestorsOrdered()`, `ancestorsOrderedDesc()`
These methods return a `BelongsToManyInTransitiveClosure` instance just as `descendants()` and `ancestors()`.
Additionally, these queries will be ordered by the topological ordering.

### Method `rebuildTopologicalOrdering()`
You can use this method to recompute the topological ordering from scratch.
When rebuilding the entire transitive closure, this method should be performed after performing `rebuildTransitiveClosure()`.
Note that because a graph can have multiple topological orderings, the rebuilt topological ordering may differ from any previously existing ordering.
