# Public API - Forest extension

- [Getting started](getting-started.md)
- [Important notes](important.md)
- API
  - [General](api-general.md)
  - [Max Vertex Degree](api-max-vertex-degree.md)
  - **[Forest (trees)](api-forest.md)**
    - [Description](#user-content-description)
    - [Examples](#user-content-examples)
    - [Trait `IsForest`](#user-content-trait-isforest)
  - [Topological ordering](api-topological-ordering.md)
  - [Self loops](api-self-loops.md)

`IsForest` - *A trait for your vertex model, specializing the DAG into a tree or forest.*

## Description
Trees are common data structures.
Multiple trees form a forest, which can be represented using the `IsForest` trait.
This trait builds upon the [Max Vertex Degree](api-max-vertex-degree.md) trait to enforce a maximum indegree of `1`.

***Note:*** *While all transitive closure algorithms will produce correct results, the `ForestAlgorithm` is especially suited for maintaining forests. Refer to [Getting started](getting-started.md) for instructions on how to choose an algorithm.*

## Examples
Given a vertex model instance `$vertex`, you can e.g.:
```php
    // Obtain all siblings
    $vertex->siblings
    
    // Or obtain all siblings including itself
    $vertex->siblingsAndSelf
    
    // Obtain the parent (singular)
    $vertex->parent
    
    // Associate a parent
    $vertex->parent()->associate($parent)
    
    // Dissociate the parent
    $vertex->parent()->dissociate()
```

## Trait `IsForest`

### Method `parent()`
The `IsVertexInDag` provides a `parents()` relation, but vertices in a forest only have one parent.
Hence, the `IsForest` trait provides an additional `parent()` relation, which uses the `HasOneParentInForest` class built upon Laravel's `HasOneThrough` relation.
This relation should be regarded as readonly, as in that any mutating methods from `HasOneThrough` should not be used.
However, `HasOneParentInForest` does provide two specialized mutating methods: `associate()` and `dissociate()`.
These methods associate or dissociate the vertex with/from a parent, similar to the behaviour of these methods in Laravel's `BelongsTo` relation.

### Methods `siblings()`, `siblingsAndSelf()`
These methods provide functionality for querying the siblings of the current vertex.
The `siblings()` method excludes the current vertex, `siblingsAndSelf()` includes this vertex.
Both methods return a `BelongsToManySiblings` instance which builds upon Laravel's `BelongsToMany` class.
Note that these relations should be regarded as fully read-only: no mutating relation methods should be used.

***NOTE:*** *These relations return the `children` of the `parent` of the given vertex. If the vertex has no `parent`, no children will be returned. In particular, for a vertex `$v` with no `parent`, `$v->siblingsAndSelf` will be empty rather than `[$v]`.*

### Method `linkDescendantChildrenAndParent()`
This method is identical to `linkDescendantChildren()`, except it also fills the `parent` relation of each of the descendants of the current vertex.
It does this using no additional database queries.

### Method `linkDescendantDescendantsAndParent()`
This method is identical to `linkDescendantDescendants()`, except just like `linkDescendantChildrenAndParent()` it also fills the `parent` relation of all descendants.

### Method `linkAncestorParent()`
This method is identical to `linkAncestorParents()`, except it also fills the `parent` relation for each of the ancestors and the current vertex.

### Method `iterateAncestorsInTree(bool $link = true)`
This method provides a `Generator` over all ancestors of the current vertex, starting with the parent.
Set `$link` to `false` to disable the automatic call to `linkAncestorParent()`, e.g. if you have performed it already.
