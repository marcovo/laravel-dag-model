# Public API - General

- [Getting started](getting-started.md)
- [Important notes](important.md)
- API
  - **[General](api-general.md)**
    - [General outline](#user-content-general-outline)
    - [Versioning](#user-content-versioning)
    - [Examples](#user-content-examples)
    - [Trait `IsVertexInDag`](#user-content-trait-isvertexindag)
    - [DAG Query Builder](#user-content-dag-query-builder)
    - [Class `BelongsToManyInGraph` (`children()`, `parents()`)](#user-content-class-belongstomanyingraph-children-parents)
      - [Graph traversal](#user-content-graph-traversal)
    - [Trait `IsEdgeInDag`](#user-content-trait-isedgeindag)
    - [Transitive closure algorithms](#user-content-transitive-closure-algorithms)
  - [Max Vertex Degree](api-max-vertex-degree.md)
  - [Forest (trees)](api-forest.md)
  - [Topological ordering](api-topological-ordering.md)
  - [Self loops](api-self-loops.md)

## General outline
When having completed the steps in [Getting started](getting-started.md), you have the basic DAG functionality available to your vertex model.
You will find the documentation on this general functionality on this page.
Additionally, you can also extend or specialize your DAG using extension traits, which are outlined in the other sections listed above.

## Versioning
Note that only methods described on these API pages are subject to the backwards compatibility promise.
For more details refer to the [Versioning](../README.md#user-content-versioning) section of the readme.

## Examples
Given a vertex model instance `$vertex`, you can e.g.:
```php
    // Obtain all children
    $vertex->children
    
    // Obtain all descendants
    $vertex->descendants
    
    // Obtain all children using a query
    $vertex->children()->get()
    
    // Attach a parent
    $vertex->parents()->attach($parent)
    
    // Or, conversely, a child
    $parent->children()->attach($vertex)

    // Detach a child
    $vertex->children()->detach($child)

    // Sync children
    $vertex->children()->sync([$child1, $child2])

    // Traverse the subgraph
    $vertex->children()->traverseDfs($callback)

    // Use the relation in a query
    $query->whereHas('children', $callback)
    $query->whereHas('ancestors', $callback)
    
    // Or use a specialized DAG query method
    $query->whereAncestorOf($vertex)
```

## Trait `IsVertexInDag`
The `\Marcovo\LaravelDagModel\Models\IsVertexInDag` trait provides general DAG functionality to your vertex model.
It consists of the following methods:

### Method `getEdgeModel()`
This is an abstract method you should implement yourself, as described in [Getting started](getting-started.md).

### Method `newEloquentBuilder()`
This package extends the eloquent builder to provide additional builder methods.
When using multiple traits that extend the builder, you can use the `\Marcovo\LaravelDagModel\Models\Builder\QueryBuilderTrait` to incorporate the DAG functionality into your own `Builder` implementation.

### Methods `children()`, `parents()`
These methods provide relations based on Laravel's `BelongsToMany` relation for accessing all children or parents of the current vertex model instance.
These relations return a `BelongsToManyInGraph` instance, which is documented in its own section.

### Methods `descendants()`, `ancestors()`
These methods provide relations based on Laravel's `BelongsToMany` relation for accessing all descendants or ancestors of the current vertex model instance.
These relations are read-only, so you should only use them to query information, not to create edges or vertices.

***NOTE:*** *Most unsupported methods have been overridden to throw a `MethodNotSupportedException`. Due to backward compatibility reasons, this is currently not possible for `firstOrCreate()`, but also this method should not be used on these relations.*

### Methods `descendantsWithSelf()`, `ancestorsWithSelf()`
These methods provide relations based on Laravel's `BelongsToMany` relation for accessing all descendants or ancestors of the current vertex model instance, also including the current vertex model itself.
These relations are read-only, so you should only use them to query information, not to create edges or vertices.
The returned `self` instance will be a fresh instance of the current vertex, i.e. it will not be the same class instance.

***NOTE:*** *Most unsupported methods have been overridden to throw a `MethodNotSupportedException`. Due to backward compatibility reasons, this is currently not possible for `firstOrCreate()`, but also this method should not be used on these relations.*

### Method `getSeparationCondition()`
You may want to store multiple DAGs in the same database table, e.g. when dealing with multiple versions or multiple independent configurations in the same database.
You can use the separation condition to let the algorithms distinguish between the different DAGs.
This function should return either `null` or a closure that can be passed to the `Builder::where()` function.
An example implementation would be:
```php
    public function getSeparationCondition(): ?\Closure
    {
        return function ($query) {
            $query->where('separation_column', '=', $this->separation_column);
        };
    }
```

### Method `newSeparatedQuery()`
This method returns a new eloquent builder instance, with the separation condition applied to it, if it is configured.

### Method `rebuildTransitiveClosure()`
You may wish to rebuild the transitive closure from the graph definition.
Situations in which this may be needed is when the transitive closure for some reason is corrupted, or when migrating from another data structure or DAG implementation.
You can rebuild the transitive closure using this method on a fully configured vertex model instance.
The rebuild algorithm adds any missing transitive closure edges, and removes edges that should not exist.
Valid existing edges remain unchanged, except for the `updated_at` column which is updated to the current timestamp.

### Method `linkDescendantChildren()`
This method eager loads all descendants of the current vertex, and in addition populates the `children` relations of all these descendants.
This is done in such a way that any occurrences of the same vertex within the descendants graph will be populated with the exact same model instance.
In this way, you are guaranteed to work with consistent models in the descendants graph when e.g. traversing through it.

Note that this method only performs two queries in total: one to fetch all descendants, and one to fetch all descendant relations.

### Method `linkDescendantDescendants()`
This method performs the same logic as `linkDescendantChildren()`, but in addition also populates the `descendants` relations of all descendants.
No extra database queries are performed to perform this task.

### Method `linkAncestorParents()`
This method is equivalent to `linkDescendantChildren()` but instead eager loads all ancestors and populates their `parents` relations.

### Method `linkAncestorAncestors()`
This method is equivalent to `linkDescendantDescendants()` but instead eager loads all ancestors and populates their `parents` and `ancestors` relations.

### Method `queryIsAncestorOf($other): bool`
Determines whether this model is an ancestor of the other model using a query.

### Method `queryIsDescendantOf($other): bool`
Determines whether this model is a descendant of the other model using a query.

### Method `queryIsParentOf($other): bool`
Determines whether this model is a parent of the other model using a query.

### Method `queryIsChildOf($other): bool`
Determines whether this model is a child of the other model using a query.

## DAG Query Builder
Query builders on the vertex model have the following additional methods at their disposal:

```php
    $query->whereAncestorOf($model)
    $query->orWhereAncestorOf($model)
    
    $query->whereAncestorOfOrSelf($model)
    $query->orWhereAncestorOfOrSelf($model)
    
    $query->whereDescendantOf($model)
    $query->orWhereDescendantOf($model)
    
    $query->whereDescendantOfOrSelf($model)
    $query->orWhereDescendantOfOrSelf($model)
```

In these methods, `$model` can be either a vertex model instance or an id.

## Class `BelongsToManyInGraph` (`children()`, `parents()`)
This relation class provides the logic for the `children()` and `parents()` relations.
This relation is partly read-only: any query mutation methods have been disabled.
These are e.g. `->children()->insert()` and `->parents()->delete()`.
These methods are disabled because all graph modifications should be done by the transitive closure algorithm, which is not possible through these query methods.
Mutation methods that operate based on model instances are supported because they can be rerouted through the algorithm.
You can e.g. use the following Laravel `BelongsToMany` mutation methods:
```php
    $vertex->children()->save()
    $vertex->children()->saveMany()
    $vertex->children()->create()
    $vertex->children()->toggle()
    $vertex->children()->sync()
    $vertex->children()->updateExistingPivot()
    $vertex->children()->attach()
    $vertex->children()->detach()
```
Additionally, all `BelongsToMany` querying methods are also available here.

### Graph traversal
The `children()` and `parents()` relations offer methods for traversing the graph starting from a given vertex.
The graph traversal methods on `->children()` will traverse over the given vertex and all its descendants.
Likewise, traversal over `->parents()` will visit the given vertex and all its ancestors.
The following traversal algorithms are implemented:
```php
    // Breadth first search
    $vertex->children()->traverseBfs($callback)

    // Depth first search, pre-ordering
    $vertex->children()->traverseDfs($callback)

    // Depth first search, post-ordering
    $vertex->children()->traverseDfsPostOrdering($callback)

    // Depth first search, pre-ordering and post-ordering
    $vertex->children()->traverseDfs($callbackPre, $callbackPost)
```
If the given callback returns a non-`null` value, the traversal is stopped and the given non-`null` value is returned to the caller.

***NOTE:*** *Traversing a subgraph without eager loading relations may cause many queries to be executed. Use a suitable `link*` method to eager load the necessary relations.*

## Trait `IsEdgeInDag`
In [Getting started](getting-started.md) you have created an edge model, which either extends the `PathCountAlgorithmEdge`, `DlswAlgorithmEdge` or `ForestAlgorithmEdge` class or implements the `IsEdgeInDagContract` interface using the `IsEdgeInDag` trait.
The following methods can be overridden to e.g. provide custom column names for the relevant edge model columns:
```php
    public function getVertexModel(): IsVertexInDagContract;

    public function getTransitiveClosureAlgorithm(): TransitiveClosureAlgorithmContract;
    
    public function getStartVertexColumn(): string;

    public function getEndVertexColumn(): string;

    public function getEdgeTypeColumn(): string;
    
    public function getPathCountColumn(): string; // Path count algorithm only
```

## Transitive closure algorithms
*For theoretical details, refer to [Important notes](important.md#user-content-transitive-closure-algorithms)*

In your edge model, you may override `getTransitiveClosureAlgorithm()` to define which algorithm is to be used.
In general, you would just return one of these:
```php
    return new DlswAlgorithm($this);
    
    return new PathCountAlgorithm($this);
    
    return new ForestAlgorithm($this);
```

The `PathCountAlgorithm` contains one additional API method, `setPotentialPathCountOverFlowDetection()`.
This method can be used to turn on/off the overflow detection, by setting it to one of the `PathCountAlgorithm::OVERFLOW_DETECTION_*` methods.
Do make sure you only turn this detection off if at the database level the overflow is detected.
