<?php

namespace Marcovo\LaravelDagModel\TransitiveClosureAlgorithms;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\MySqlGrammar;
use Illuminate\Database\Query\Grammars\PostgresGrammar;
use Illuminate\Database\Query\Grammars\SQLiteGrammar;
use Marcovo\LaravelDagModel\Exceptions\CycleException;
use Marcovo\LaravelDagModel\Exceptions\DuplicateEdgeException;
use Marcovo\LaravelDagModel\Exceptions\EdgeNotFoundException;
use Marcovo\LaravelDagModel\Exceptions\LaravelDagModelException;
use Marcovo\LaravelDagModel\Exceptions\LogicException;
use Marcovo\LaravelDagModel\Exceptions\PotentialPathCountOverflowException;
use Marcovo\LaravelDagModel\Exceptions\SelfLinkException;
use Marcovo\LaravelDagModel\Exceptions\UnsupportedDatabaseDriverException;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

/**
 * @internal (Except OVERFLOW_DETECTION_* and setPotentialPathCountOverFlowDetection())
 */
class PathCountAlgorithm extends AbstractTransitiveClosureAlgorithm implements TransitiveClosureAlgorithmContract
{
    /** @api */
    public const OVERFLOW_DETECTION_OFF = 'off';

    /** @api */
    public const OVERFLOW_DETECTION_NAIVE = 'naive';

    /*
    |--------------------------------------------------------------------------
    | Potential path count overflow detection
    |--------------------------------------------------------------------------
    |
    | When using the PathCount algorithm, certain vertically large graphs may
    | contain large paths counts. If this path count exceeds the maximum integer
    | that can be stored (assumed to be 2^64 - 1), the algorithm will break.
    |
    | With this option enabled, the algorithm will check for any potential overflow
    | before adding any edges. If you are using a database configuration that
    | correctly detects integer overflows and throws an exception in such a case,
    | this detection may be turned off.
    |
    | Possible values:
    |  * 'off' - perform no detection
    |  * 'naive' - throw an exception whenever path counts are so high an overflow may happen
    |
    | Note: it may be possible in the future to include another option that can check whether
    | overflow WILL happen instead of that it MAY happen.
    |
    */
    private string $detectPotentialPathCountOverflow = self::OVERFLOW_DETECTION_NAIVE;

    /**
     * @api
     */
    public function setPotentialPathCountOverFlowDetection(string $value): self
    {
        if (
            !in_array($value, [
                self::OVERFLOW_DETECTION_OFF,
                self::OVERFLOW_DETECTION_NAIVE,
            ])
        ) {
            throw new LogicException('Invalid value for setPotentialPathCountOverFlowDetection(): ' . $value);
        }

        $this->detectPotentialPathCountOverflow = $value;

        return $this;
    }

    public function getManagedColumns(): array
    {
        return [
            $this->pivot->getStartVertexColumn(),
            $this->pivot->getEndVertexColumn(),
            $this->pivot->getPathCountColumn(),
            $this->pivot->getEdgeTypeColumn(),
        ];
    }

    public function getAlgorithmSpecificColumns(): array
    {
        return [
            $this->pivot->getPathCountColumn(),
            $this->pivot->getEdgeTypeColumn(),
        ];
    }

    public function beforeRebuild()
    {
        $this->selectEdgesQuery(false)->update([
            $this->pivot->getPathCountColumn() => 0,
        ]);
    }

    /**
     * @throws LaravelDagModelException
     */
    public function createEdge($startVertex, $endVertex)
    {
        $this->connection->transaction(function () use ($startVertex, $endVertex) {
            if ($startVertex === $endVertex) {
                throw new SelfLinkException();
            }

            if ($this->separationCallback !== null) {
                $this->checkSeparation($startVertex, $endVertex);
            }

            /**
             * We do not allow an edge to be created twice. Note that an edge may be present already
             * in the transitive closure, if this is the case we do allow adding the edge, and said
             * edge will be promoted to a graph edge.
             */
            if ($this->rebuildThreshold === null && $this->hasGraphEdge($startVertex, $endVertex)) {
                throw new DuplicateEdgeException();
            }

            /**
             * Cycles are not allowed in an acyclic graph
             */
            if ($this->hasEdge($endVertex, $startVertex)) {
                throw new CycleException();
            }

            $this->checkForPotentialPathCountOverflow();

            /**
             * Insert the graph edge if it does not exist yet (it may exist in the TC). We will update
             * the path_count and edge_type fields later.
             */
            $now = Carbon::now()->toDateTimeString();
            $this->connection->table($this->edgesTable)->insertOrIgnore([
                $this->pivot->getStartVertexColumn() => $startVertex,
                $this->pivot->getEndVertexColumn() => $endVertex,
                $this->pivot->getPathCountColumn() => 0,
                $this->pivot->getEdgeTypeColumn() => IsEdgeInDagContract::TYPE_CLOSURE_EDGE,
                $this->pivot->getCreatedAtColumn() => $now,
                $this->pivot->getUpdatedAtColumn() => $now,
            ]);

            /**
             * Create the transitive closure edges
             */
            $this->createTcEdgesExtendingFromStart($startVertex, $endVertex, [
                $this->pivot->getPathCountColumn() => $this->connection->raw('0'),
            ]);
            $this->createTcEdgesExtendingFromEnd($startVertex, $endVertex, [
                $this->pivot->getPathCountColumn() => $this->connection->raw('0'),
            ]);
            $this->createTcEdgesExtendingFromStartAndEnd($startVertex, $endVertex, [
                $this->pivot->getPathCountColumn() => $this->connection->raw('0'),
            ]);

            /**
             * Update path counts of all edges in the transitive closure that enclose the current edge
             */
            $this->updatePathCounts($startVertex, $endVertex, true);

            /**
             * Update the graph edge with correct values:
             *  - it is a graph edge
             *  - add 1 to path_count because the graph edge itself is also a path
             */
            $this->connection->table($this->edgesTable)
                ->where($this->pivot->getStartVertexColumn(), '=', $startVertex)
                ->where($this->pivot->getEndVertexColumn(), '=', $endVertex)
                ->update([
                    $this->pivot->getEdgeTypeColumn() => IsEdgeInDagContract::TYPE_GRAPH_EDGE,
                    $this->pivot->getPathCountColumn() => $this->connection->raw(
                        $this->connection->getQueryGrammar()->wrap($this->pivot->getPathCountColumn()) . ' + 1'
                    ),
                    $this->pivot->getUpdatedAtColumn() => $now,
                ]);
        });
    }

    /**
     * @throws LaravelDagModelException
     */
    public function deleteEdge($startVertex, $endVertex)
    {
        $this->connection->transaction(function () use ($startVertex, $endVertex) {
            /**
             * Check that the edge to be deleted exists, and is a graph edge
             */
            $edge = $this->getGraphEdge($startVertex, $endVertex);
            if ($edge === null) {
                throw new EdgeNotFoundException();
            }

            /**
             * Update path counts of all edges in the transitive closure that enclose the edge to be deleted
             */
            $this->updatePathCounts($startVertex, $endVertex, false);

            /**
             * Update the graph edge with correct values:
             *  - it is not a graph edge anymore
             *  - remove 1 from path_count because the graph edge itself was also a path
             */
            $keyName = $this->pivot->getKeyName();
            $this->connection->table($this->edgesTable)
                ->where($keyName, '=', $edge->$keyName)
                ->update([
                    $this->pivot->getEdgeTypeColumn() => IsEdgeInDagContract::TYPE_CLOSURE_EDGE,
                    $this->pivot->getPathCountColumn() => $this->connection->raw(
                        $this->connection->getQueryGrammar()->wrap($this->pivot->getPathCountColumn()) . ' - 1'
                    ),
                    $this->pivot->getUpdatedAtColumn() => Carbon::now()->toDateTimeString(),
                ]);

            /**
             * Edges with path_count = 0 have no paths accounting for them, so we need to delete those
             */
            $this->connection->table($this->edgesTable)
                ->where($this->pivot->getPathCountColumn(), '=', 0)
                ->when($this->hasSelfLoops(), fn (Builder $query) => $query
                    ->where($this->pivot->getEdgeTypeColumn(), '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE))
                ->delete();
        });
    }

    private function checkForPotentialPathCountOverflow()
    {
        if ($this->detectPotentialPathCountOverflow === self::OVERFLOW_DETECTION_NAIVE) {
            $rows = $this->selectEdgesQuery()
                ->orderByDesc($this->pivot->getPathCountColumn())
                ->limit(3)
                ->pluck($this->pivot->getPathCountColumn());

            /**
             * If the count is lower than 3, we have at most 2 edges in the transitive closure, which can
             * never lead to overflow. When it is 3, the maximum value that might be attained by adding a
             * new edge is given by a + b * c. If after calculating this, PHP returns it as a double, the
             * int has overflown the 64 bit range.
             */
            if (count($rows) === 3 && is_double($rows[2] + $rows[1] * $rows[0])) {
                throw new PotentialPathCountOverflowException();
            }
        }
    }

    private function updatePathCounts($startVertex, $endVertex, bool $add)
    {
        $grammar = $this->connection->getQueryGrammar();

        $operator = $add ? '+' : '-';
        $quotedEdgesTable = $grammar->wrap($this->edgesTable);

        if ($grammar instanceof MySqlGrammar) {
            $whereAliasA = 'a';
            $setAliasPrefixA = 'a.';
        } elseif ($grammar instanceof PostgresGrammar || $grammar instanceof SQLiteGrammar) {
            $whereAliasA = $quotedEdgesTable;
            $setAliasPrefixA = '';
        } else {
            throw new UnsupportedDatabaseDriverException(); // @codeCoverageIgnore
        }

        $wrappedStartVertexName = $grammar->wrap($this->pivot->getStartVertexColumn());
        $wrappedEndVertexName = $grammar->wrap($this->pivot->getEndVertexColumn());
        $wrappedEdgeTypeName = $grammar->wrap($this->pivot->getEdgeTypeColumn());
        $wrappedPathCountName = $grammar->wrap($this->pivot->getPathCountColumn());
        $wrappedUpdatedAt = $grammar->wrap($this->pivot->getUpdatedAtColumn());

        $thresholdA = $thresholdB = $thresholdC = '';
        if ($this->rebuildThreshold !== null) {
            $quotedRebuildThreshold = $grammar->quoteString($this->rebuildThreshold);
            $thresholdA = ' AND ' . $whereAliasA . '.' . $wrappedUpdatedAt . ' >= ' . $quotedRebuildThreshold;
            $thresholdB = ' AND b.' . $wrappedUpdatedAt . ' >= ' . $quotedRebuildThreshold;
            $thresholdC = ' AND c.' . $wrappedUpdatedAt . ' >= ' . $quotedRebuildThreshold;
        }

        $whereNotSelfLoopB = $whereNotSelfLoopC = '';
        if ($this->hasSelfLoops()) {
            $whereNotSelfLoopB = ' AND b.' . $wrappedEdgeTypeName . ' != ' . IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE;
            $whereNotSelfLoopC = ' AND c.' . $wrappedEdgeTypeName . ' != ' . IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE;
        }

        $this->updatePathCountsQuery(
            $quotedEdgesTable,
            $quotedEdgesTable . ' b',
            $setAliasPrefixA . $wrappedPathCountName
                . ' = ' . $whereAliasA . '.' . $wrappedPathCountName . ' '
                . $operator . ' b.' . $wrappedPathCountName,
            '
                b.' . $wrappedEndVertexName . ' = ' . $this->quoteVertexKey($startVertex) . '
                AND ' . $whereAliasA . '.' . $wrappedStartVertexName . ' = b.' . $wrappedStartVertexName . '
                AND ' . $whereAliasA . '.' . $wrappedEndVertexName . ' = ' . $this->quoteVertexKey($endVertex) . '
            ' . $whereNotSelfLoopB . $thresholdA . $thresholdB
        );

        $this->updatePathCountsQuery(
            $quotedEdgesTable,
            $quotedEdgesTable . ' b',
            $setAliasPrefixA . $wrappedPathCountName
                . ' = ' . $whereAliasA . '.' . $wrappedPathCountName . ' '
                . $operator . ' b.' . $wrappedPathCountName,
            '
                b.' . $wrappedStartVertexName . ' = ' . $this->quoteVertexKey($endVertex) . '
                AND ' . $whereAliasA . '.' . $wrappedEndVertexName . ' = b.' . $wrappedEndVertexName . '
                AND ' . $whereAliasA . '.' . $wrappedStartVertexName . ' = ' . $this->quoteVertexKey($startVertex) . '
            ' . $whereNotSelfLoopB . $thresholdA . $thresholdB
        );

        $this->updatePathCountsQuery(
            $quotedEdgesTable,
            $quotedEdgesTable . ' b, ' . $quotedEdgesTable . ' c',
            $setAliasPrefixA . $wrappedPathCountName
                . ' = ' . $whereAliasA . '.' . $wrappedPathCountName . ' '
                . $operator . ' b.' . $wrappedPathCountName . ' * c.' . $wrappedPathCountName,
            '
                b.' . $wrappedEndVertexName . ' = ' . $this->quoteVertexKey($startVertex) . '
                AND c.' . $wrappedStartVertexName . ' = ' . $this->quoteVertexKey($endVertex) . '
                AND ' . $whereAliasA . '.' . $wrappedStartVertexName . ' = b.' . $wrappedStartVertexName . '
                AND ' . $whereAliasA . '.' . $wrappedEndVertexName . ' = c.' . $wrappedEndVertexName . '
            ' . $whereNotSelfLoopB . $whereNotSelfLoopC . $thresholdA . $thresholdB . $thresholdC
        );
    }

    private function updatePathCountsQuery(string $modifyTable, string $otherTables, string $set, string $where)
    {
        $grammar = $this->connection->getQueryGrammar();

        if ($grammar instanceof MySqlGrammar) {
            $this->connection->statement('
                UPDATE ' . $modifyTable . ' a, ' . $otherTables . '
                SET ' . $set . '
                WHERE ' . $where . '
            ');
        } elseif ($grammar instanceof PostgresGrammar || $grammar instanceof SQLiteGrammar) {
            $this->connection->statement('
                UPDATE ' . $modifyTable . '
                SET ' . $set . '
                FROM ' . $otherTables . '
                WHERE ' . $where . '
            ');
        } else {
            throw new UnsupportedDatabaseDriverException(); // @codeCoverageIgnore
        }
    }

    public function createSelfLoopEdge($vertex, array $extra = []): void
    {
        parent::createSelfLoopEdge($vertex, [
            $this->pivot->getPathCountColumn() => 0,
        ]);
    }
}
