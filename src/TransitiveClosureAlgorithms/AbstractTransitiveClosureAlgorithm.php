<?php

namespace Marcovo\LaravelDagModel\TransitiveClosureAlgorithms;

use Carbon\Carbon;
use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\MySqlGrammar;
use Illuminate\Database\Query\Grammars\PostgresGrammar;
use Illuminate\Database\Query\Grammars\SQLiteGrammar;
use Marcovo\LaravelDagModel\Exceptions\LaravelDagModelException;
use Marcovo\LaravelDagModel\Exceptions\LogicException;
use Marcovo\LaravelDagModel\Exceptions\SeparationException;
use Marcovo\LaravelDagModel\Exceptions\UnsupportedDatabaseDriverException;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\Models\Extensions\WithSelfLoops;

/**
 * @internal (Except __construct())
 */
abstract class AbstractTransitiveClosureAlgorithm implements TransitiveClosureAlgorithmContract
{
    protected Connection $connection;
    protected string $edgesTable;
    /** @var Pivot|IsEdgeInDagContract */
    protected IsEdgeInDagContract $pivot;

    protected ?\Closure $separationCallback = null;
    protected ?string $rebuildThreshold = null;

    /**
     * @api
     */
    public function __construct(IsEdgeInDagContract $pivot)
    {
        $this->connection = $pivot->getConnection();
        $this->pivot = $pivot;
        $this->edgesTable = $pivot->getTable();
    }

    public function getPivotInstance(): IsEdgeInDagContract
    {
        return $this->pivot;
    }

    public function copy(): self
    {
        $copy = new static($this->pivot);

        $copy->separationCallback = $this->separationCallback;
        $copy->rebuildThreshold = $this->rebuildThreshold;

        return $copy;
    }

    protected function hasSelfLoops(): bool
    {
        return in_array(WithSelfLoops::class, class_uses_recursive($this->pivot->getVertexModel()));
    }

    protected function quoteVertexKey($key)
    {
        if ($this->pivot->getVertexModel()->getKeyType() === 'int') {
            return (int)$key;
        }

        return $this->connection->getQueryGrammar()->quoteString($key);
    }

    public function rebuild()
    {
        $algorithm = $this->copy();

        $updated_at = $algorithm->selectEdgesQuery(false)->max($this->pivot->getUpdatedAtColumn());

        $now = Carbon::now();
        if ($updated_at !== null) {
            if ($now->is($updated_at)) {
                usleep(1_100_000);
                $now = Carbon::now();
            } elseif ($now->lte($updated_at)) {
                throw new LaravelDagModelException('Cannot rebuild a TC with an updated_at in the future');
            }
        }
        $algorithm->rebuildThreshold = $now->toDateTimeString();

        $algorithm->connection->transaction(function () use ($algorithm, $now) {
            $algorithm->beforeRebuild();

            $startVertexName = $this->pivot->getStartVertexColumn();
            $endVertexName = $this->pivot->getEndVertexColumn();

            $algorithm->selectEdgesQuery(false)
                ->where($this->pivot->getEdgeTypeColumn(), '=', IsEdgeInDagContract::TYPE_GRAPH_EDGE)
                ->chunkById(50, function ($edges) use ($algorithm, $startVertexName, $endVertexName) {
                    foreach ($edges as $edge) {
                        $algorithm->createEdge($edge->$startVertexName, $edge->$endVertexName);
                    }
                });

            $algorithm->selectEdgesQuery(false)
                ->where($this->pivot->getUpdatedAtColumn(), '<', $now)
                ->when($this->hasSelfLoops(), fn (Builder $query) => $query
                    ->where($this->pivot->getEdgeTypeColumn(), '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE))
                ->delete();
        });
    }

    public function beforeRebuild()
    {
    }

    public function setSeparationCallback(\Closure $separationCallback)
    {
        $this->separationCallback = $separationCallback;
    }

    protected function selectEdgesQuery(bool $addRebuildThreshold = true): Builder
    {
        $query = $this->connection->table($this->edgesTable);

        if ($this->separationCallback !== null) {
            $query->where(($this->separationCallback)($this->edgesTable, $this->pivot->getStartVertexColumn()));
        }

        if ($addRebuildThreshold && $this->rebuildThreshold !== null) {
            $query->where($this->pivot->getUpdatedAtColumn(), '>=', $this->rebuildThreshold);
        }

        return $query;
    }

    public function hasEdge($startVertex, $endVertex): bool
    {
        $where = [
            [$this->pivot->getStartVertexColumn(), $startVertex],
            [$this->pivot->getEndVertexColumn(), $endVertex],
        ];

        if ($this->hasSelfLoops()) {
            $where[] = [$this->pivot->getEdgeTypeColumn(), '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE];
        }

        return $this->selectEdgesQuery()->where($where)->exists();
    }

    public function hasGraphEdge($startVertex, $endVertex): bool
    {
        return $this->selectEdgesQuery()->where([
            [$this->pivot->getStartVertexColumn(), $startVertex],
            [$this->pivot->getEndVertexColumn(), $endVertex],
            [$this->pivot->getEdgeTypeColumn(), IsEdgeInDagContract::TYPE_GRAPH_EDGE],
        ])->exists();
    }

    public function getEdge($startVertex, $endVertex): ?\stdClass
    {
        return $this->selectEdgesQuery()->where([
            [$this->pivot->getStartVertexColumn(), $startVertex],
            [$this->pivot->getEndVertexColumn(), $endVertex],
        ])->first();
    }

    public function getGraphEdge($startVertex, $endVertex): ?\stdClass
    {
        return $this->selectEdgesQuery()->where([
            [$this->pivot->getStartVertexColumn(), $startVertex],
            [$this->pivot->getEndVertexColumn(), $endVertex],
            [$this->pivot->getEdgeTypeColumn(), IsEdgeInDagContract::TYPE_GRAPH_EDGE],
        ])->first();
    }

    protected function checkSeparation($startVertex, $endVertex)
    {
        $vertexCount = $this->connection->query()
            ->from(
                $this->connection->query()->selectRaw($this->quoteVertexKey($startVertex) . ' as vertex')
                    ->union($this->connection->query()->selectRaw($this->quoteVertexKey($endVertex) . ' as vertex')),
                'vertices'
            )
            ->where(($this->separationCallback)('vertices', 'vertex'))
            ->count();

        if ($vertexCount !== 2) {
            throw new SeparationException();
        }
    }

    /**
     * Create transitive closure edges (A, E) for paths (A, S) extending from the start vertex S of given edge (S, E)
     *
     *     E
     *     ^
     *     |
     *     |
     *     S
     *     ^
     *      \
     *       \
     *        A
     */
    protected function createTcEdgesExtendingFromStart($startVertex, $endVertex, array $extra = []): void
    {
        $grammar = $this->connection->getQueryGrammar();
        $now = Carbon::now()->toDateTimeString();

        $select = $this->connection->table($this->edgesTable)
            ->select(array_merge([
                $this->pivot->getStartVertexColumn(),
                $this->connection->raw($this->quoteVertexKey($endVertex)),
                $this->connection->raw(IsEdgeInDagContract::TYPE_CLOSURE_EDGE),
                $this->connection->raw($grammar->quoteString($now)),
                $this->connection->raw($grammar->quoteString($now)),
            ], array_values($extra)))
            ->where($this->pivot->getEndVertexColumn(), '=', $startVertex)
            ->when($this->hasSelfLoops(), fn (Builder $query) => $query
                ->where($this->pivot->getEdgeTypeColumn(), '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE))
        ;

        if ($this->rebuildThreshold !== null) {
            $select->where($this->pivot->getUpdatedAtColumn(), '>=', $this->rebuildThreshold);
        }

        $this->executeInsert($select, $extra);
    }

    /**
     * Create transitive closure edges (S, B) for paths (E, B) extending from the end vertex E of given edge (S, E)
     *
     *         B
     *        ^
     *       /
     *      /
     *     E
     *     ^
     *     |
     *     |
     *     S
     */
    protected function createTcEdgesExtendingFromEnd($startVertex, $endVertex, array $extra = []): void
    {
        $grammar = $this->connection->getQueryGrammar();
        $now = Carbon::now()->toDateTimeString();

        $select = $this->connection->table($this->edgesTable)
            ->select(array_merge([
                $this->connection->raw($this->quoteVertexKey($startVertex)),
                $this->pivot->getEndVertexColumn(),
                $this->connection->raw(IsEdgeInDagContract::TYPE_CLOSURE_EDGE),
                $this->connection->raw($grammar->quoteString($now)),
                $this->connection->raw($grammar->quoteString($now)),
            ], array_values($extra)))
            ->where($this->pivot->getStartVertexColumn(), '=', $endVertex)
            ->when($this->hasSelfLoops(), fn (Builder $query) => $query
                ->where($this->pivot->getEdgeTypeColumn(), '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE))
        ;

        if ($this->rebuildThreshold !== null) {
            $select->where($this->pivot->getUpdatedAtColumn(), '>=', $this->rebuildThreshold);
        }

        $this->executeInsert($select, $extra);
    }

    /**
     * Create transitive closure edges (A, B) for paths (A, S) and (E, B) extending from both vertices of
     * given edge (S, E)
     *
     *         B
     *        ^
     *       /
     *      /
     *     E
     *     ^
     *     |
     *     |
     *     S
     *     ^
     *      \
     *       \
     *        A
     */
    protected function createTcEdgesExtendingFromStartAndEnd($startVertex, $endVertex, array $extra = []): void
    {
        $grammar = $this->connection->getQueryGrammar();
        $now = Carbon::now()->toDateTimeString();

        $select = $this->connection->table($this->edgesTable . ' as a')
            ->select(array_merge([
                'a.' . $this->pivot->getStartVertexColumn(),
                'b.' . $this->pivot->getEndVertexColumn(),
                $this->connection->raw(IsEdgeInDagContract::TYPE_CLOSURE_EDGE),
                $this->connection->raw($grammar->quoteString($now)),
                $this->connection->raw($grammar->quoteString($now)),
            ], array_values($extra)))->crossJoin($this->edgesTable . ' as b')
            ->where('a.' . $this->pivot->getEndVertexColumn(), '=', $startVertex)
            ->where('b.' . $this->pivot->getStartVertexColumn(), '=', $endVertex)
            ->when($this->hasSelfLoops(), fn (Builder $query) => $query
                ->where('a.' . $this->pivot->getEdgeTypeColumn(), '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE)
                ->where('b.' . $this->pivot->getEdgeTypeColumn(), '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE))
        ;

        if ($this->rebuildThreshold !== null) {
            $select
                ->where('a.' . $this->pivot->getUpdatedAtColumn(), '>=', $this->rebuildThreshold)
                ->where('b.' . $this->pivot->getUpdatedAtColumn(), '>=', $this->rebuildThreshold);
        }

        $this->executeInsert($select, $extra);
    }

    private function executeInsert(Builder $select, array $extra): void
    {
        $grammar = $this->connection->getQueryGrammar();

        $columns = '(' . implode(', ', $grammar->wrapArray(array_merge([
                $this->pivot->getStartVertexColumn(),
                $this->pivot->getEndVertexColumn(),
                $this->pivot->getEdgeTypeColumn(),
                $this->pivot->getCreatedAtColumn(),
                $this->pivot->getUpdatedAtColumn(),
            ], array_keys($extra)))) . ')';

        $wrappedStartVertex = $grammar->wrap($this->pivot->getStartVertexColumn());
        $wrappedEndVertex = $grammar->wrap($this->pivot->getEndVertexColumn());
        $wrappedUpdatedAt = $grammar->wrap($this->pivot->getUpdatedAtColumn());
        $quotedCurrentDatetime = $grammar->quoteString(Carbon::now()->toDateTimeString());

        if ($grammar instanceof MySqlGrammar) {
            $insertQuery = '
                INSERT INTO ' . $grammar->wrap($this->edgesTable) . ' ' . $columns . ' ' . $select->toSql() . '
                ON DUPLICATE KEY UPDATE ' . $wrappedUpdatedAt . ' = ' . $quotedCurrentDatetime;
        } elseif ($grammar instanceof PostgresGrammar) {
            $insertQuery = '
                INSERT INTO ' . $grammar->wrap($this->edgesTable) . ' ' . $columns . ' ' . $select->toSql() . '
                ON CONFLICT (' . $wrappedStartVertex . ', ' . $wrappedEndVertex . ')
                DO UPDATE SET ' . $wrappedUpdatedAt . ' = ' . $quotedCurrentDatetime;
        } elseif ($grammar instanceof SQLiteGrammar) {
            $insertQuery = '
                INSERT INTO ' . $grammar->wrap($this->edgesTable) . ' ' . $columns . ' ' . $select->toSql() . '
                ON CONFLICT (' . $wrappedStartVertex . ', ' . $wrappedEndVertex . ')
                DO UPDATE SET ' . $wrappedUpdatedAt . ' = ' . $quotedCurrentDatetime;
        } else {
            throw new UnsupportedDatabaseDriverException(); // @codeCoverageIgnore
        }

        $this->connection->insert($insertQuery, $select->getBindings());
    }

    /**
     * Create self-loop edge (V, V) for the vertex V
     */
    public function createSelfLoopEdge($vertex, array $extra = []): void
    {
        if (!$this->hasSelfLoops()) {
            throw new LogicException(); // @codeCoverageIgnore
        }

        $now = Carbon::now()->toDateTimeString();
        $this->connection->table($this->pivot->getTable())->insert(array_merge([
            $this->pivot->getStartVertexColumn() => $vertex,
            $this->pivot->getEndVertexColumn() => $vertex,
            $this->pivot->getEdgeTypeColumn() => IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE,
            $this->pivot->getCreatedAtColumn() => $now,
            $this->pivot->getUpdatedAtColumn() => $now,
        ], $extra));
    }

    /**
     * Delete self-loop edge (V, V) for the vertex V
     */
    public function deleteSelfLoopEdge($vertex): void
    {
        if (!$this->hasSelfLoops()) {
            throw new LogicException(); // @codeCoverageIgnore
        }

        $this->connection->table($this->pivot->getTable())->where([
            $this->pivot->getStartVertexColumn() => $vertex,
            $this->pivot->getEndVertexColumn() => $vertex,
            $this->pivot->getEdgeTypeColumn() => IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE,
        ])->delete();
    }
}
