<?php

namespace Marcovo\LaravelDagModel\Exceptions;

use Throwable;

/**
 * @api
 */
class UnsupportedDatabaseDriverException extends LaravelDagModelException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if ($message === '') {
            $message = 'Unsupported database driver';
        }

        parent::__construct($message, $code, $previous);
    }
}
