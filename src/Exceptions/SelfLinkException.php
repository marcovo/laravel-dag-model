<?php

namespace Marcovo\LaravelDagModel\Exceptions;

use Throwable;

/**
 * @api
 */
class SelfLinkException extends LaravelDagModelException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if ($message === '') {
            $message = 'Cannot create an edge from a vertex to itself';
        }

        parent::__construct($message, $code, $previous);
    }
}
