<?php

namespace Marcovo\LaravelDagModel\Exceptions;

use Exception;

/**
 * @api
 */
class LaravelDagModelException extends Exception
{
}
