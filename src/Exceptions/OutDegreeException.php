<?php

namespace Marcovo\LaravelDagModel\Exceptions;

/**
 * @api
 */
class OutDegreeException extends VertexDegreeException
{
    public static function make(int $max): self
    {
        return new static('Vertex already has ' . $max . ' children');
    }
}
