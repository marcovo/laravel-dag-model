<?php

namespace Marcovo\LaravelDagModel\Models\Edge;

use Marcovo\LaravelDagModel\Exceptions\LaravelDagModelException;
use Marcovo\LaravelDagModel\Exceptions\LogicException;
use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;
use Marcovo\LaravelDagModel\Models\IsVertexInDag;
use Marcovo\LaravelDagModel\Models\IsVertexInDagContract;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\TransitiveClosureAlgorithmContract;

/**
 * @api
 */
trait IsEdgeInDag
{
    /**
     * @api
     * @return IsVertexInDagContract|IsVertexInDag
     */
    abstract public function getVertexModel(): IsVertexInDagContract;

    /**
     * @api
     */
    public function getStartVertexColumn(): string
    {
        return 'start_vertex';
    }

    /**
     * @api
     */
    public function getEndVertexColumn(): string
    {
        return 'end_vertex';
    }

    /**
     * @api
     */
    public function getEdgeTypeColumn(): string
    {
        return 'edge_type';
    }

    /**
     * @internal
     */
    public function save(array $options = [])
    {
        /** @var TransitiveClosureAlgorithmContract $algorithm */
        $algorithm = $this->pivotParent->getConfiguredTransitiveClosureAlgorithm();

        if ($this->exists) {
            /**
             * If the row already exists, we just save the modified columns, except
             * we don't allow modifying any of the managed columns
             */
            if ($this->isDirty($algorithm->getManagedColumns())) {
                throw new MethodNotSupportedException();
            }

            return parent::save($options);
        }

        /**
         * This model does not exist, so it is a new graph edge. It may already be
         * present as a transitive closure edge, so care should be taken.
         * First check none of the algorithm specific columns are wrongfully set.
         */
        if ($this->isDirty($algorithm->getAlgorithmSpecificColumns())) {
            throw new MethodNotSupportedException();
        }

        $exception = new class extends LaravelDagModelException {
        };

        try {
            $return = $this->getConnection()->transaction(function () use ($algorithm, $options, $exception) {
                /**
                 * Fire a custom event as a substitute for the saving event
                 */
                if ($this->fireModelEvent('creatingDagEdge') === false) {
                    return false;
                }

                $startVertexName = $this->getStartVertexColumn();
                $endVertexName = $this->getEndVertexColumn();

                $algorithm->createEdge($this->$startVertexName, $this->$endVertexName);

                /**
                 * After having created the edge, we update the original data of our model
                 * as if the row already existed before calling save()
                 */
                $originalModel = self::query()
                    ->setModel($this)
                    ->where($startVertexName, $this->$startVertexName)
                    ->where($endVertexName, $this->$endVertexName)
                    ->first([
                        $this->getKeyName(),
                        ... $algorithm->getManagedColumns(),
                    ]);

                if ($originalModel === null) {
                    $additional = (env('DB_CONNECTION') === 'mariadb')
                        ? ' Did you try to link a nonexistent vertex?' : '';
                    throw new LogicException('Could not find inserted edge.' . $additional);
                }

                $this->exists = true;
                $this->original = $originalModel->getAttributes();

                /**
                 * If there are any dirty fields left, now is the time to update those. The
                 * exception mechanism makes sure whenever the saving event returns false,
                 * any changes are rolled back and we will also return false from save().
                 */
                if (!parent::save($options)) {
                    throw new $exception();
                }

                return null;
            });

            if ($return === false) {
                // TODO: This return is untested
                return false;
            }
        } catch (LaravelDagModelException $e) {
            if ($e instanceof $exception) {
                return false;
            } else {
                throw $e;
            }
        }

        return true;
    }

    /**
     * @internal
     */
    public function delete()
    {
        return $this->getConnection()->transaction(function () {
            if ($this->fireModelEvent('deleting') === false) {
                return 0;
            }

            $this->touchOwners();

            $startVertexName = $this->getStartVertexColumn();
            $endVertexName = $this->getEndVertexColumn();

            // Note: after deleting the edge, the edge row may still be present
            // as a transitive closure edge
            $this->pivotParent->getConfiguredTransitiveClosureAlgorithm()->deleteEdge(
                $this->$startVertexName,
                $this->$endVertexName
            );

            $this->exists = false;

            $this->fireModelEvent('deleted', false);

            return 1;
        });
    }
}
