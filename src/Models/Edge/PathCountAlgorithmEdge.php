<?php

namespace Marcovo\LaravelDagModel\Models\Edge;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\PathCountAlgorithm;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\TransitiveClosureAlgorithmContract;

/**
 * @api
 */
abstract class PathCountAlgorithmEdge extends Pivot implements IsEdgeInDagContract
{
    use IsEdgeInDag;

    public $incrementing = true;

    /**
     * @internal
     */
    public function getTransitiveClosureAlgorithm(): TransitiveClosureAlgorithmContract
    {
        return new PathCountAlgorithm($this);
    }

    /**
     * @api
     */
    public function getPathCountColumn(): string
    {
        return 'path_count';
    }
}
