<?php

namespace Marcovo\LaravelDagModel\Models\Relations;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\TransitiveClosureAlgorithmContract;

class BelongsToManySiblings extends BelongsToManyInGraph
{
    private bool $andSelf;
    private TransitiveClosureAlgorithmContract $transitiveClosureAlgorithm;

    /**
     * @internal
     */
    public function __construct(
        Builder $query,
        Model $parent,
        $table,
        $foreignPivotKey,
        $relatedPivotKey,
        $parentKey,
        $relatedKey,
        $relationName,
        TransitiveClosureAlgorithmContract $transitiveClosureAlgorithm,
        bool $andSelf
    ) {
        $this->andSelf = $andSelf;
        $this->transitiveClosureAlgorithm = $transitiveClosureAlgorithm;

        parent::__construct(
            $query,
            $parent,
            $table,
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey,
            $relatedKey,
            $relationName,
            $transitiveClosureAlgorithm
        );
    }

    protected function performJoin($query = null)
    {
        parent::performJoin($query);

        $query = $query ?: $this->query;

        $this->performParentJoin($query);

        return $this;
    }

    private function performParentJoin($query)
    {
        /**
         * The mental picture:
         *                       parent
         *             parentKey o    o parentKey
         *       foreignPivotKey o    o foreignPivotKey
         *                      /      \
         *       __parent_join /        \ children/normal pivot
         *                    /          \
         *   relatedPivotKey o            o relatedPivotKey
         *        relatedKey o            o relatedKey
         *                vertex        sibling
         */
        $query
            ->join(
                $this->getTable() . ' as __parent_join',
                '__parent_join.' . $this->foreignPivotKey,
                '=',
                $this->getQualifiedForeignPivotKeyName()
            )
            ->where(
                '__parent_join.' . $this->transitiveClosureAlgorithm->getPivotInstance()->getEdgeTypeColumn(),
                '=',
                IsEdgeInDagContract::TYPE_GRAPH_EDGE
            );
    }

    protected function addWhereConstraints()
    {
        $this->query->where(
            '__parent_join.' . $this->relatedPivotKey,
            '=',
            $this->parent->{$this->relatedKey}
        );

        if (!$this->andSelf) {
            $this->query->where(
                $this->related->getTable() . '.' . $this->relatedKey,
                '!=',
                $this->parent->{$this->relatedKey}
            );
        }

        return $this;
    }

    public function addEagerConstraints(array $models)
    {
        $whereIn = $this->whereInMethod($this->parent, $this->parentKey);

        $this->query->{$whereIn}(
            '__parent_join.' . $this->relatedPivotKey,
            $this->getKeys($models, $this->parentKey)
        );
    }

    protected function aliasedPivotColumns()
    {
        $columns = parent::aliasedPivotColumns();

        $columns[] = '__parent_join.' . $this->relatedPivotKey . ' as __parent_join_related_pivot_key';

        return $columns;
    }

    public function match(array $models, Collection $results, $relation)
    {
        parent::match($models, $results, $relation);

        if (!$this->andSelf) {
            /** @var Model $model */
            foreach ($models as $model) {
                if ($model->relationLoaded($relation)) {
                    $related = $model->getRelation($relation);
                    $related = $related->where($this->relatedKey, '!=', $model->{$this->relatedKey})->values();
                    $model->setRelation($relation, $related);
                }
            }
        }

        return $models;
    }

    protected function buildDictionary(Collection $results)
    {
        $dictionary = [];

        foreach ($results as $result) {
            $dictionary[$result->__parent_join_related_pivot_key][] = $result;
        }

        return $dictionary;
    }

    public function getRelationExistenceQuery(Builder $query, Builder $parentQuery, $columns = ['*'])
    {
        $query->select($columns);

        $query->from($this->related->getTable() . ' as ' . $hash = $this->getRelationCountHash());

        $this->related->setTable($hash);

        $this->performJoin($query);

        $query->select($columns)->whereColumn(
            $this->getQualifiedParentKeyName(),
            '=',
            '__parent_join.' . $this->relatedPivotKey
        );

        if (!$this->andSelf) {
            $query->whereColumn(
                $hash . '.' . $this->relatedKey,
                '!=',
                '__parent_join.' . $this->relatedPivotKey
            );
        }

        return $query;
    }

    /**
     * @internal
     */
    public function newPivotQuery()
    {
        throw new MethodNotSupportedException();
    }

    // TODO: Uncomment whenever we drop support for laravel < 9
    // public function firstOrCreate(array $attributes = [], array $values = [], array $joining = [], $touch = true)
    // {
    //     throw new MethodNotSupportedException();
    // }

    /**
     * @internal
     */
    public function updateOrCreate(array $attributes, array $values = [], array $joining = [], $touch = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function save(Model $model, array $pivotAttributes = [], $touch = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function saveMany($models, array $pivotAttributes = [])
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function create(array $attributes = [], array $joining = [], $touch = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function createMany(iterable $records, array $joinings = [])
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function toggle($ids, $touch = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function sync($ids, $detaching = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function updateExistingPivot($id, array $attributes, $touch = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function attach($id, array $attributes = [], $touch = true)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function detach($ids = null, $touch = true)
    {
        throw new MethodNotSupportedException();
    }
}
