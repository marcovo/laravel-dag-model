<?php

namespace Marcovo\LaravelDagModel\Models\Relations;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;
use Marcovo\LaravelDagModel\TransitiveClosureAlgorithms\TransitiveClosureAlgorithmContract;

class BelongsToManyInTransitiveClosureWithoutSelfLoops extends BelongsToManyInTransitiveClosure
{
    /**
     * @internal
     */
    public function __construct(
        Builder $query,
        Model $parent,
        $table,
        $foreignPivotKey,
        $relatedPivotKey,
        $parentKey,
        $relatedKey,
        $relationName,
        TransitiveClosureAlgorithmContract $transitiveClosureAlgorithm
    ) {
        parent::__construct(
            $query,
            $parent,
            $table,
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey,
            $relatedKey,
            $relationName,
            $transitiveClosureAlgorithm
        );

        $pivot = $transitiveClosureAlgorithm->getPivotInstance();

        $this
            ->where($pivot->qualifyColumn($pivot->getEdgeTypeColumn()), '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE);
    }

    public function newPivotStatement()
    {
        $pivot = new $this->using();

        return parent::newPivotStatement()
            ->where($pivot->qualifyColumn($pivot->getEdgeTypeColumn()), '!=', IsEdgeInDagContract::TYPE_SELF_LOOP_EDGE);
    }
}
