<?php

namespace Marcovo\LaravelDagModel\Models\Relations;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Marcovo\LaravelDagModel\Exceptions\MethodNotSupportedException;

class HasOneParentInForest extends HasOneThrough
{
    private string $relationName;

    /**
     * @internal
     */
    public function __construct(
        Builder $query,
        Model $farParent,
        Model $throughParent,
        $firstKey,
        $secondKey,
        $localKey,
        $secondLocalKey,
        string $relationName
    ) {
        $this->relationName = $relationName;

        parent::__construct($query, $farParent, $throughParent, $firstKey, $secondKey, $localKey, $secondLocalKey);
    }

    /**
     * @internal
     */
    public function firstOrCreate(array $attributes = [], array $values = [])
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function create(array $attributes = [])
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function firstOrNew(array $attributes = [], array $values = [])
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function insert(array $values)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function insertOrIgnore(array $values)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function insertGetId(array $values, $sequence = null)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function insertUsing(array $columns, $query)
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function updateOrInsert(array $attributes, array $values = [])
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function delete()
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @internal
     */
    public function forceDelete()
    {
        throw new MethodNotSupportedException();
    }

    /**
     * @api
     */
    public function associate($parent)
    {
        $this->parent->getConnection()->transaction(function () use ($parent) {
            $current = $this->farParent->{$this->relationName}()->first();

            if ($current !== null) {
                if ($parent instanceof Model) {
                    if ($current->is($parent)) {
                        return;
                    }
                } else {
                    if ($parent === $current->{$this->secondKey}) {
                        return;
                    }
                }

                $this->farParent->parents()->detach($current);
            }

            $this->farParent->parents()->attach($parent);

            if ($parent instanceof Model) {
                $this->farParent->setRelation($this->relationName, $parent);
            } else {
                $this->farParent->unsetRelation($this->relationName);
            }
        });

        return $this->farParent;
    }

    /**
     * @api
     */
    public function dissociate()
    {
        $current = $this->farParent->{$this->relationName}()->first();

        if ($current !== null) {
            $this->farParent->parents()->detach($current);

            $this->farParent->setRelation($this->relationName, null);
        }

        return $this->farParent;
    }
}
