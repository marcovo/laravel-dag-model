<?php

namespace Marcovo\LaravelDagModel\Models\Extensions;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Grammars\MySqlGrammar;
use Illuminate\Database\Query\Grammars\PostgresGrammar;
use Illuminate\Database\Query\Grammars\SQLiteGrammar;
use Illuminate\Support\Facades\Event;
use Marcovo\LaravelDagModel\Exceptions\LogicException;
use Marcovo\LaravelDagModel\Exceptions\UnsupportedDatabaseDriverException;
use Marcovo\LaravelDagModel\Models\DagVertexModel;
use Marcovo\LaravelDagModel\Models\IsVertexInDag;
use Marcovo\LaravelDagModel\Models\Relations\BelongsToManyInTransitiveClosure;

/**
 * @mixin DagVertexModel
 */
trait WithTopologicalOrdering
{
    /**
     * @api
     */
    public function getTopologicalOrderingColumn(): string
    {
        return 'top_order';
    }

    public static function bootWithTopologicalOrdering()
    {
        static::creating(function (self $vertex) {
            $orderingColumn = $vertex->getTopologicalOrderingColumn();

            $max = $vertex->newSeparatedQuery()->max($orderingColumn);

            $vertex->$orderingColumn = $max !== null ? $max + 1 : 0;
        });

        static::deleted(function (self $vertex) {
            $orderingColumn = $vertex->getTopologicalOrderingColumn();

            $vertex->newSeparatedQuery()
                ->where($orderingColumn, '>=', $vertex->$orderingColumn)
                ->decrement($orderingColumn);
        });

        $edgeClass = (new static())->children()->getPivotClass();
        Event::listen(
            'eloquent.creatingDagEdge: ' . $edgeClass,
            function (Model $edge) {
                static::reorderForNewGraphEdge($edge);
            }
        );
    }

    /**
     * @api
     */
    public function scopeOrderTopologically(Builder $query, string $direction = 'asc')
    {
        $query->orderBy($this->getTopologicalOrderingColumn(), $direction);
    }

    /**
     * @api
     */
    public function descendantsOrdered(): BelongsToManyInTransitiveClosure
    {
        $pivot = $this->getEdgeModel();
        return $this
            ->belongsToManyInTransitiveClosure($pivot->getStartVertexColumn(), $pivot->getEndVertexColumn())
            ->orderTopologically();
    }

    /**
     * @api
     */
    public function ancestorsOrdered(): BelongsToManyInTransitiveClosure
    {
        $pivot = $this->getEdgeModel();
        return $this
            ->belongsToManyInTransitiveClosure($pivot->getEndVertexColumn(), $pivot->getStartVertexColumn())
            ->orderTopologically();
    }

    /**
     * @api
     */
    public function ancestorsOrderedDesc(): BelongsToManyInTransitiveClosure
    {
        $pivot = $this->getEdgeModel();
        return $this
            ->belongsToManyInTransitiveClosure($pivot->getEndVertexColumn(), $pivot->getStartVertexColumn())
            ->orderTopologically('desc');
    }

    private static function reorderForNewGraphEdge(Model $edge)
    {
        $startVertexId = $edge->{$edge->getStartVertexColumn()};
        $endVertexId = $edge->{$edge->getEndVertexColumn()};
        $orderingColumn = (new static())->getTopologicalOrderingColumn();

        /** @var Collection|IsVertexInDag[] $vertices */
        $vertices = static::find($startVertexId)
            ->newSeparatedQuery()
            ->findMany([$startVertexId, $endVertexId])
            ->keyBy((new static())->getKeyName());

        if (count($vertices) !== 2) {
            throw new LogicException('Could not find all vertices'); // @codeCoverageIgnore
        }

        $startVertex = $vertices[$startVertexId];
        $endVertex = $vertices[$endVertexId];

        if ($startVertex->$orderingColumn < $endVertex->$orderingColumn) {
            // The two vertices are already in the right order in the topological ordering,
            // so we do not have to reorder
            return;
        }

        // The start vertex currently comes after the end vertex in the topological ordering,
        // so we will have to swap them. We will need to make sure that any existing relations
        // will remain correctly ordered. Hence, we will be taking the start vertex and all its
        // ancestors that come after the end vertex, and move these before the end vertex.
        $ancestors = $startVertex->ancestors()
            ->where($orderingColumn, '>', $endVertex->$orderingColumn)
            ->orderBy($orderingColumn)
            ->get();

        // Suppose we have start vertex S, end vertex E, ancestors (of S) A1, A2, A3, A4, A5 and
        // other vertices V1, V2, V3. Then the situation on the next line needs to be transformed
        // into the subsequent line
        //     E A1 A2 V1 A3 V2 A4 A5 V3 S
        //  ->
        //     A1 A2 A3 A4 A5 S E V1 V2 V3

        $distance = $startVertex->$orderingColumn - $endVertex->$orderingColumn;
        $numAncestors = $ancestors->count();
        $numOthers = $distance - $numAncestors - 1;
        $newStartValue = $endVertex->$orderingColumn + $numAncestors;
        $newEndValue = $newStartValue + 1;

        // We start by dividing the vertices into ranges, e.g. in our example:
        //     [A1, A2], [V1], [A3], [V2], [A4, A5], [V3]

        $ranges = [];
        $rangeStart = null;
        $ancestorsCursor = 0;
        $isAncestorRange = null;
        for ($i = $endVertex->$orderingColumn + 1; $i <= $startVertex->$orderingColumn; $i++) {
            if ($ancestorsCursor < $numAncestors && $ancestors[$ancestorsCursor]->$orderingColumn == $i) {
                $ancestor = $ancestors[$ancestorsCursor];
                $ancestorsCursor++;
            } else {
                $ancestor = null;
            }

            if (
                $rangeStart === null // First iteration
                || $i == $startVertex->$orderingColumn // Last iteration
                || $isAncestorRange !== ($ancestor !== null) // Range barrier
            ) {
                if ($rangeStart !== null) {
                    $ranges[] = [$isAncestorRange, $rangeStart, $i - 1];
                }

                $rangeStart = $i;
                $isAncestorRange = $ancestor !== null;
            }
        }

        if ($ancestorsCursor !== $numAncestors) {
            throw new LogicException('Detected corrupt ordering'); // @codeCoverageIgnore
        }

        // Now we compute for each of the ranges what the new ordering positions should be. We do this by
        // building an UPDATE query expression setting:
        //     top_order = top_order + delta
        // where delta consists of 1 IF() per range plus 1 IF() for each of the start and end vertices
        $grammar = $startVertex->getConnection()->getQueryGrammar();
        $wrapOrderCol = $grammar->wrap($orderingColumn);

        $offsetAncestors = $offsetOthers = 0;
        $delta = $wrapOrderCol;
        $delta .= ' + (
            CASE WHEN ' . $wrapOrderCol . ' = ' . ((int)$startVertex->$orderingColumn) . '
                THEN -' . ($numOthers + 1) . '
                ELSE 0
            END
        )';
        $delta .= ' + (
            CASE WHEN ' . $wrapOrderCol . ' = ' . ((int)$endVertex->$orderingColumn) . '
                THEN ' . ($numAncestors + 1) . '
                ELSE 0
            END
        )';
        foreach ($ranges as [$isAncestorRange, $rangeStart, $rangeEnd]) {
            if ($isAncestorRange) {
                // Move between old E and new S
                $rangeDelta = $endVertex->$orderingColumn - $rangeStart + $offsetAncestors;
                $offsetAncestors += $rangeEnd - $rangeStart + 1;
            } else {
                // Move between new E and old S
                $rangeDelta = $newEndValue + 1 - $rangeStart + $offsetOthers;
                $offsetOthers += $rangeEnd - $rangeStart + 1;
            }

            $delta .= ' + (
                CASE WHEN ' . $rangeStart . ' <= ' . $wrapOrderCol . ' AND ' . $wrapOrderCol . ' <= ' . $rangeEnd . '
                    THEN ' . $rangeDelta . '
                    ELSE 0
                END
            )';
        }

        // Perform the update
        $startVertex->newSeparatedQuery()
            ->where($orderingColumn, '>=', $endVertex->$orderingColumn)
            ->where($orderingColumn, '<=', $startVertex->$orderingColumn)
            ->update([
                $orderingColumn => $startVertex->getConnection()->raw($delta),
            ]);
    }

    /**
     * @api
     */
    public function rebuildTopologicalOrdering()
    {
        $this->getConnection()->transaction(function () {
            $pivot = $this->getEdgeModel();
            $orderingColumn = $this->getTopologicalOrderingColumn();

            $grammar = $this->getConnection()->getQueryGrammar();

            // First we set some arbitrary ordering for the vertices
            $wrappedTempName = $grammar->wrap('rebuild_top_order_' . uniqid());
            if ($grammar instanceof MySqlGrammar) {
                $this->newSeparatedQuery()->update([
                    $orderingColumn => $this->getConnection()->raw('@i:=(COALESCE(@i, -1)+1)'),
                ]);
            } elseif ($grammar instanceof PostgresGrammar) {
                $this->getConnection()->statement('CREATE SEQUENCE ' . $wrappedTempName . ';');
                $this->newSeparatedQuery()->update([
                    $orderingColumn => $this->getConnection()->raw("nextval('" . $wrappedTempName . "')"),
                ]);
                $this->newSeparatedQuery()->decrement($orderingColumn);
                $this->getConnection()->statement('DROP SEQUENCE ' . $wrappedTempName . ';');
            } elseif ($grammar instanceof SQLiteGrammar) {
                $wrappedKey = $grammar->wrap($this->getKeyName());
                $wrappedTable = $grammar->wrap($this->getTable());
                $this->getConnection()->statement('
                    CREATE TEMPORARY TABLE ' . $wrappedTempName . ' AS
                        SELECT ' . $wrappedKey . ', row_number() over (order by ' . $wrappedKey . ') rn
                        FROM ' . $wrappedTable . '
                ');
                $this->newSeparatedQuery()->update([
                    $orderingColumn => $this->getConnection()->raw('(
                        SELECT rn
                        FROM ' . $wrappedTempName . '
                        WHERE ' . $wrappedTempName . '.' . $wrappedKey . ' = ' . $wrappedTable . '.' . $wrappedKey . '
                    )')
                ]);
                $this->newSeparatedQuery()->decrement($orderingColumn);
                $this->getConnection()->statement('DROP TABLE ' . $wrappedTempName . ';');
            } else {
                throw new UnsupportedDatabaseDriverException(); // @codeCoverageIgnore
            }

            // Next we order the vertices edge-by-edge
            $edgeQuery = $this->children()->getPivotClass()::query();
            $this->applySeparationConditionToEdgesQuery(
                $edgeQuery,
                $pivot->getTable(),
                $pivot->getStartVertexColumn()
            );
            $edgeQuery->chunkById(50, function ($edges) {
                foreach ($edges as $edge) {
                    static::reorderForNewGraphEdge($edge);
                }
            });
        });
    }
}
