<?php

namespace Marcovo\LaravelDagModel\Models\Extensions;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Event;
use Marcovo\LaravelDagModel\Exceptions\InDegreeException;
use Marcovo\LaravelDagModel\Exceptions\LogicException;
use Marcovo\LaravelDagModel\Exceptions\OutDegreeException;
use Marcovo\LaravelDagModel\Models\IsVertexInDag;

/**
 * @mixin Model
 * @mixin IsVertexInDag
 */
trait WithMaxVertexDegree
{
    /**
     * @api
     */
    public function maxInDegree(): ?int
    {
        return null;
    }

    /**
     * @api
     */
    public function maxOutDegree(): ?int
    {
        return null;
    }

    public static function bootWithMaxVertexDegree()
    {
        $edgeClass = (new static())->children()->getPivotClass();
        Event::listen(
            'eloquent.creatingDagEdge: ' . $edgeClass,
            function (Model $edge) {
                static::checkVertexDegrees($edge);
            }
        );
    }

    private static function checkVertexDegrees(Model $edge)
    {
        $startVertexId = $edge->{$edge->getStartVertexColumn()};
        $endVertexId = $edge->{$edge->getEndVertexColumn()};

        /** @var Collection|WithMaxVertexDegree[] $vertices */
        $vertices = static::find($startVertexId)
            ->newSeparatedQuery()
            ->findMany([$startVertexId, $endVertexId])
            ->keyBy((new static())->getKeyName());

        if (count($vertices) !== 2) {
            throw new LogicException('Could not find all vertices'); // @codeCoverageIgnore
        }

        $startVertex = $vertices[$startVertexId];
        $endVertex = $vertices[$endVertexId];

        $maxOutDegree = $startVertex->maxOutDegree();
        if ($maxOutDegree !== null) {
            if ($startVertex->children()->count() + 1 > $maxOutDegree) {
                throw OutDegreeException::make($maxOutDegree);
            }
        }

        $maxInDegree = $endVertex->maxInDegree();
        if ($maxInDegree !== null) {
            if ($endVertex->parents()->count() + 1 > $maxInDegree) {
                throw InDegreeException::make($maxInDegree);
            }
        }
    }
}
