<?php

namespace Marcovo\LaravelDagModel\Models;

use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

/**
 * @api
 */
interface IsVertexInDagContract
{
    public function getEdgeModel(): IsEdgeInDagContract;
}
