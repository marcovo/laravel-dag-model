<?php

namespace Marcovo\LaravelDagModel\Models;

use Illuminate\Database\Eloquent\Model;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDag;
use Marcovo\LaravelDagModel\Models\Edge\IsEdgeInDagContract;

abstract class DagVertexModel extends Model implements IsVertexInDagContract
{
    use IsVertexInDag;

    /**
     * @api
     * @return IsEdgeInDagContract|IsEdgeInDag
     */
    abstract public function getEdgeModel(): IsEdgeInDagContract;
}
