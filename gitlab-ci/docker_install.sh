#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install mariadb-client libpq-dev -yqq

# Install database drivers
docker-php-ext-install pdo_mysql pdo_pgsql

phpver=`php -r "echo PHP_VERSION;"`
if [[ $phpver =~ 7\.4\.[0-9]+ ]]; then
  pecl install xdebug-3.1.6
else
  pecl install xdebug
fi
docker-php-ext-enable xdebug

bash ./gitlab-ci/install_composer.sh
